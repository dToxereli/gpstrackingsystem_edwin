$(init)

function init() {
    $('a.partial-link').each(function (index) {
        $(this).click(function(e) {
            e.preventDefault();
            var url = this.href;
            var container = partialcontainers[$(this).data('container')];            
            $.get(url, function (response) {                                
                if(container != null)
                    $(container).html(response);
                else
                    console.log('Error loading partial');
            });
        }); 
    });

    $('a.partial-btn').each(function (index) {
        $(this).click(function(e) {
            e.preventDefault();
            var url = $(this).data('source');
            var container = partialcontainers[$(this).data('container')];            
            $.get(url, function (response) {                                
                if(container != null)
                    $(container).html(response);
                else
                    console.log('Error loading partial');
            });
        }); 
    });
    
    $('button.partial-link').each(function(index){    
        $(this).click(function(e) {
            e.preventDefault();
            var url = $(this).data('source');
            var container = partialcontainers[$(this).data('container')];
            $.get(url, function (response) {                
                if(container != null)
                    $(container).html(response);
                else
                    console.log('Error loading partial');
            });
        });
    });

    $('form.partial-form').each(function (index) {        
        $form = $(this);
        var url = $form.attr('action');
        var container = partialcontainers[$(this).data('container')];
        var method = $form.attr('method');        
        $form.submit( function (e) {      
            var data = $form.serialize();            
            e.preventDefault();
            $.ajaxSetup({
                header:$('meta[name="_token"]').attr('content')
            });            
            $.ajax({
                type: method,
                url: url,
                data: data,
                dataType: 'html',
                success: function (response) {                    
                    if(container != null)
                        $(container).html(response);
                    else
                        console.log('Error loading partial');
                }
            });
        });
    });

    $('.data-table').each(function (index) {
        $(this).DataTable();
    });
}