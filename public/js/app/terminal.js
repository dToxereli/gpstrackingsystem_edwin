var terminalList = {};

function Driver(name, phone, email) {
    this.name = name;
    this.phone = phone;
    this.email = email;
}

function Terminal(data) {
    this.online = false;
    this.signal = false;
    this.imei = data.imei;

    if (data['lastLocation']) {
        this.location = {
            lat: data['lastLocation']['latitude'], lng: data['lastLocation']['longitude']
        };
    } else {
        this.location = {
            lat: null, lng: null
        };
    }
    this.imei = data.imei;
    this.name = data['display'];
    this.marker = null;
    // this.onlineIndicator = document.getElementById(data.imei+'-status');
    this.liveData = {
        speed: 0,
        temperature: 0,
        enginestate: "Off",
        oil1: 0,
        oil2: 0,
        bearing: null,
        altitude: null
    };

    this.lastStopData = {
        distanceSince: 0,
        time: "",
        location: {
            lat: null, lng: null
        }
    };

    if (data.driver) {
        this.driver = new Driver(data.driver.name, data.driver.phone, data.driver.email);
    } else {
        this.driver = new Driver("","","");
    }
}

Terminal.prototype.getName = function () {
    return this.name;
}

Terminal.prototype.setOnlineStatus = function newStatus(s) {
    if (this.online == s) {
        return;
    }
    this.online = s;
    if (s) {
        document.getElementById(this.imei+'-status').className = 'online';
        if (this.marker) {
            this,this.marker.setIcon(icons.BLUE);
        }
    } else {
        document.getElementById(this.imei+'-status').className = 'offline';
        if (this.marker) {
            this,this.marker.setIcon(icons.RED);
        }
    }
}

Terminal.prototype.setPos = function (pos) {
    this.location = pos;
    if (this.marker) {
        if (!pos || !pos.lat || !pos.lng) {
            this,this.marker.setIcon(icons.DARK);
            this.signal = false;
        } else {
            var gpos = new google.maps.LatLng(pos.lat, pos.lng);
            this.marker.setPosition(pos);
            this.signal = true;
        }
    }
    console.log(pos);
};

Terminal.prototype.getPos = function () {
    var pos = this.location;
    if (this.marker) {
        return new google.maps.LatLng(pos.lat, pos.lng);
    } else {
        return pos;
    }
};

Terminal.prototype.newLiveData = function (data) {
    
    if (data.gpsSignalPresent) {
        this.setPos({
            lat: data.latitude, lng: data.longitude
        });
    }
    this.setOnlineStatus(data.gpsSignalPresent);
    this.liveData.speed = data.speed;
    
    if (data.speed > 0) {
        this.move();
    } else {
        this.stop();
    }

    this.liveData.temperature = data.temperature;
    this.liveData.oil1 = data.oilPercent1;
    this.liveData.oil2 = data.oilPercent2;
    this.liveData.enginestate = (data.accState == "1") ? "On" : "Off" ;

    this.liveData.bearing = data.directionOrRequest;
    this.liveData.altitude = data.altitude;

    this.lastStopData.distanceSince += data.distance;
}

Terminal.prototype.stop = function () {
    this.lastStopData.distanceSince = 0;
    this.lastStopData.location = this.location;
    var d = new Date();
    this.lastStopData.time = d.toLocaleString();
    if (this.marker) {
        this,this.marker.setIcon(icons.GREEN);
    }
}

Terminal.prototype.move = function () {
    if (this.marker) {
        this,this.marker.setIcon(icons.BLUE);
    }
}

Terminal.prototype.showOn = function (map) { 
    this.marker.setMap (map);  
};

Terminal.prototype.hide = function () { 
    this.marker.setMap (null);  
};

Terminal.prototype.setDriver = function(driver) {
    if (driverlist.hasOwnProperty(driver)) {
        this.driver = driver;
    }
};

Terminal.prototype.setMarker = function (map, infowindow, location) {
    if (!location) {
        location = this.location;
    }
    var terminal = this;
    
    var pos = new google.maps.LatLng(location.lat, location.lng);
    var marker = new google.maps.Marker({
        position: pos,
        map: map,
        title: terminal.name,
        icon: terminal.online ? icons.BLUE : icons.RED
    });
    google.maps.event.addListener(marker, 'click', function() {
        var content = getInfoWindowContent(terminal);
        infowindow.setContent(content);
        infowindow.open(map,marker);
    });
    this.marker = marker;
};

function getInfoWindowContent(t)
{
    return '' +
        '<div id="iw-container">' +
        '<div class="iw-title">' + t.name + '</div>' +
        '<div class="iw-content">' +
        '<div class="iw-subTitle">Location details</div>' +
        '<div><b>Current location: </b>' + t.location.lat + "," + t.location.lng + '</div>' +
        '<div><b>Bearing: </b>' + t.liveData.bearing + '</div>' +
        '<div><b>Altitude: </b>' + t.liveData.altitude + '</div>' +
        '<div class="iw-subTitle">Live data</div>' +
        '<div><b>Speed:</b>' + t.liveData.speed + '</div>' +
        '<div><b>Temperature:</b>' + t.liveData.temperature + '</div>' +
        '<div><b>Oil 1:</b>' + t.liveData.oil1 + '</div>' +
        '<div><b>Oil 2:</b>' + t.liveData.oil2 + '</div>' +
        '<div><b>Engine state:</b>' + t.liveData.enginestate + '</div>' +
        '<div class="iw-subTitle">Last stop</div>' +
        '<div><b>Time:</b>' + t.lastStopData.time + '</div>' +
        '<div><b>Location: </b>' + t.lastStopData.location.lat + "," + t.lastStopData.location.lng + '</div>' +
        '<div class="iw-subTitle">Driver details</div>' +
        '<div><b>Name:</b>' + t.driver.name + '</div>' +
        '<div><b>Phone:</b>' + t.driver.phone + '</div>' +
        '<div><b>Email:</b>' + t.driver.email + '</div>' +
        '</div>' +
        '</div>';
}