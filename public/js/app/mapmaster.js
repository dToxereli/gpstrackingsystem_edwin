var terminals;
var map;
var route;
var infowindow;
var markerlist = new Object();
var geocoder = null;

function initMap() {
    var mapelement = document.getElementById('map');
    var myLatLng = new google.maps.LatLng(-1, 37);
    map = new google.maps.Map(mapelement, {
        center: myLatLng,
        zoom: 8
    });

    infowindow = new google.maps.InfoWindow({
        content: null
    });

    geocoder = new google.maps.Geocoder;

    /*
    * The google.maps.event.addListener() event waits for
    * the creation of the infowindow HTML structure 'domready'
    * and before the opening of the infowindow defined styles
    * are applied.
    */
    google.maps.event.addListener(infowindow, 'domready', function() {
        // Reference to the DIV which receives the contents of the infowindow using jQuery
        
        // Reference to the DIV which receives the contents of the infowindow using jQuery
        var iwOuter = $('.gm-style-iw');

        /* The DIV we want to change is above the .gm-style-iw DIV.
            * So, we use jQuery and create a iwBackground variable,
            * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
            */
        var iwBackground = iwOuter.prev();

        // Remove the background shadow DIV
        iwBackground.children(':nth-child(2)').css({'display' : 'none'});

        // Remove the white background DIV
        iwBackground.children(':nth-child(4)').css({'display' : 'none'});

        iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6', 'z-index': '1'});

        var iwCloseBtn = iwOuter.next();

        // Apply the desired effect to the close button
        iwCloseBtn.css({
        opacity: '1', // by default the close button has an opacity of 0.7
        right: '60px', top: '20px', // button repositioning
        border: '2px solid #e90000', // increasing button border and new color
        'border-radius': '50%', // circular effect
        width: '1em', height: '1em',
        'background-color' : '#f00',
        'text-align' : 'center',
        'font-size' : '18px'
        });

        iwCloseBtn.html('');

        // The API automatically applies 0.7 opacity to the button after the mouseout event.
        // This function reverses this event to the desired value.
        iwCloseBtn.mouseout(function(){
            $(this).css({opacity: '1'});
        });

    });

    initMarkers();
}

function initMarkers() {
    var imeis = Object.keys(visterminals);
    imeis.forEach(imei => {
        var terminal = visterminals[imei];
        var pos = new google.maps.LatLng(terminal['lastLocation'].lat, terminal['lastLocation'].lng);
        var marker = new google.maps.Marker({
            position: pos,
            map: map,
            title: terminal['name'],
            icon: icons.BLUE
        });
        terminal['marker'] = marker;
        google.maps.event.addListener(marker, 'click', function() {
            var content = ''+
            '<div id="iw-container">' +
                '<div class="iw-title">'+terminal['name']+'</div>' +
                '<div class="iw-content">' +
                    '<div class="iw-subTitle">Terminal details</div>' +
                    '<div><b>Current location: </b>'+terminal['location']+'</div>' +
                '</div>'+
            '</div>'
            infowindow.setContent(content);
            infowindow.open(map,marker);
        });
    });

    socket.on('location data', function(msg) {
        var imei = msg['imei'];
        if ( msg.gpsSignalPresent == 0 || !msg.longitude || !msg.latitude) {
            return;
        }
        var pos = new google.maps.LatLng(msg.latitude, msg.longitude);
        var data = {
            speed: msg['speed'],
            oil1: msg['oil1'],
            oil2: msg['oil2'],
            temperature: msg['temperature'],
            timeSinceLastStop: msg['timeSinceLastStop'],
            distanceSinceLastStop: msg['distanceSinceLastStop'],
            workingTime: msg['workingTime'],
        };
        if (visterminals.hasOwnProperty(imei)) {
            visterminals[imei]['marker'].setPosition(pos);
            visterminals[imei]['position'] = pos;
            visterminals[imei]['data'] = data;
        }
    });
}

function drawPolyLine(path)
{
    route = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: 'hsl(180, 50%, 60%)',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    route.setMap(map);
}

function removePolyline()
{
    if (route) {
        route.setMap(null);
        route = null;
    }
}

function geocodeLatLng(latlng, location, callback) {
    if(!geocoder || !latlng) return "Unknown location";

    //Method to actually do the check from google maps api
    geocoder.geocode({ "location": latlng }, function (results, status) {
        if (status === "OK") { // Check if searched returned successfully
            if (results[0]) { // check if first element of results is non-null
                callback (results[0].formatted_address);
            } else {
                callback ("Unknown location");
            }
        } else {
            console.log("Geocoder failed due to: " + status);
            callback ("Unknown location");
        }
    });
}