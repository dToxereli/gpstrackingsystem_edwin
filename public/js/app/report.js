function prepareChart (info, element) {
    if (!info || !info.valid) {
        return;
    }

    var data = new google.visualization.DataTable();

    info.coldefs.forEach(element => {
        data.addCOlumn(element.type, element.name);
    });

    data.addRows(info.data);
    var chart;
    switch (info.type) {
        case 'Column':
            chart = new google.visualization.ColumnChart(element);
            break;
    
        default:
            break;
    }

    if (chart) {
        chart.draw(data, info.options);
    }
}