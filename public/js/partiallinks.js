/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 50);
/******/ })
/************************************************************************/
/******/ ({

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(51);


/***/ }),

/***/ 51:
/***/ (function(module, exports) {

var partialcontainers = {};
(function ($) {
    $.fn.partialLoader = function (action) {
        if (action == 'autoload') {
            $('#loading-screen').fadeIn('fast');
            var state = $(this).data('state');
            if (state == 'done') {
                return;
            }
            var url = $(this).data('source');
            var container = $('.partial-container[data-name=' + $(this).data('container') + ']');
            var mode = $(this).data('mode');
            $.ajax({
                url: url,
                dataType: 'html',
                timeout: 15000,
                beforeSend: function beforeSend() {
                    $('#loading-screen').fadeIn('fast');
                }
            }).done(function (data, status) {
                if (container != null) {
                    if (mode == 'append') {
                        container.append(data);
                    } else if (mode == 'prepend') {
                        container.prepend(data);
                    } else {
                        container.html(data);
                    }
                } else flash_message('Browser could not load the requested page', 'Error loading page', 'SYSTEM', 'ERROR');
            }).fail(function (data, status, error) {
                flash_message('Server responded with error code ' + data.status, error, 'SERVER', 'ERROR');
            }).always(function (data) {
                $('#loading-screen').fadeOut('fast');
            });
            $(this).data('state', 'done');
        } else if (action == 'link') {
            $(this).click(function (e) {
                e.preventDefault();
                var url = this.href;
                var container = $('.partial-container[data-name=' + $(this).data('container') + ']');
                var mode = $(this).data('mode');

                $.ajax({
                    url: url,
                    dataType: 'html',
                    timeout: 15000,
                    beforeSend: function beforeSend() {
                        $('#loading-screen').fadeIn('fast');
                    }
                }).done(function (data, status) {
                    if (container != null) {
                        if (mode == 'append') {
                            container.append(data);
                        } else if (mode == 'prepend') {
                            container.prepend(data);
                        } else {
                            container.html(data);
                        }
                    } else flash_message('Browser could not load the requested page', 'Error loading page', 'SYSTEM', 'ERROR');
                }).fail(function (data, status, error) {
                    flash_message('Server responded with error code ' + data.status, error, 'SERVER', 'ERROR');
                }).always(function (data) {
                    $('#loading-screen').fadeOut('fast');
                });
            });
        } else if (action == 'button') {
            $(this).click(function (e) {
                e.preventDefault();
                var url = $(this).data('source');
                var container = $('.partial-container[data-name=' + $(this).data('container') + ']');
                var mode = $(this).data('mode');

                $.ajax({
                    url: url,
                    dataType: 'html',
                    timeout: 15000,
                    beforeSend: function beforeSend() {
                        $('#loading-screen').fadeIn('fast');
                    }
                }).done(function (data, status) {
                    if (container != null) {
                        if (mode == 'append') {
                            container.append(data);
                        } else if (mode == 'prepend') {
                            container.prepend(data);
                        } else {
                            container.html(data);
                        }
                    } else flash_message('Browser could not load the requested page', 'Error loading page', 'SYSTEM', 'ERROR');
                }).fail(function (data, status, error) {
                    flash_message('Server responded with error code ' + data.status, error, 'SERVER', 'ERROR');
                }).always(function (data) {
                    $('#loading-screen').fadeOut('fast');
                });
            });
        } else if (action == 'form') {
            $(this).submit(function (e) {
                e.preventDefault();
                $form = $(this);
                var url = $form.attr('action');
                var container = $('.partial-container[data-name=' + $(this).data('container') + ']');
                var mode = $(this).data('mode');
                var method = $form.attr('method');
                var data = $form.serialize();
                $.ajaxSetup({
                    header: $('meta[name="_token"]').attr('content')
                });
                $.ajax({
                    type: method,
                    url: url,
                    data: data,
                    dataType: 'html',
                    beforeSend: function beforeSend() {
                        $('#loading-screen').fadeIn('fast');
                    }
                }).done(function (data, status) {
                    if (container != null) {
                        if (mode == 'append') {
                            container.append(data);
                        } else if (mode == 'prepend') {
                            container.prepend(data);
                        } else {
                            container.html(data);
                        }
                    } else flash_message('Browser could not load the requested page', 'Error loading page', 'SYSTEM', 'ERROR');
                }).fail(function (data, status, error) {
                    flash_message('Server responded with error code ' + data.status, error, 'SERVER', 'ERROR');
                }).always(function (data) {
                    $('#loading-screen').fadeOut('fast');
                });
            });
        } else if (action == 'remove') {
            $(this).click(function (e) {
                e.preventDefault();
                var mode = $(this).data('mode');
                if (mode == 'parent') {
                    $(this).parent().remove();
                } else if (mode == 'self') {
                    $(this).remove();
                } else {
                    var container = $($(this).data('target'));
                    container.remove();
                }
            });
        }
    };
})(jQuery);

/***/ })

/******/ });