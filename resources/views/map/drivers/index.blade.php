<div>
    <div class="table-responsive">
        <table class="table table-striped data-table-control name responsive" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Driver</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Terminal</th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
                <tr>                    
                    <th>Driver</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Terminal</th>
                    <th></th>                 
                </tr>
            </tfoot>
            <tbody>
                @foreach (drivers as driver)
                <tr class="terminal-entry" data-imei="{{ isset($driver->terminal) ? $driver->terminal->imei : null }}">                        
                    
                    <td>
                        <span id="{{ isset($driver->terminal) ? $driver->terminal->imei : null }}-status" class="offline">
                            <i class=" online-marker glyphicon glyphicon-signal"></i>
                        </span>
                        &nbsp;
                        {{ driver->name }}
                    </td>
                    <td>{{ (driver->description) ? driver->description : 'No description'}}</td>
                    <td>{{ (driver->identifier) ? driver->identifier : 'No Identifier'}}</td>
                    <td>{{ (driver->imei) ? driver->imei : 'No IMEI'}}</td>
                    <td>{{ (driver->terminal_group) ? driver->terminal_group->name : 'Ungrouped'}}</td>
                    <td>{{ (driver->user) ? driver->user->username : 'Not assigned' }}</td>                        
                    <td></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    $('.data-table-control').DataTable( {
        order: [[4, 'asc']],
        rowGroup: {
            dataSrc: 4
        },
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   -1
        } ],        
        select: true
    } );
</script>