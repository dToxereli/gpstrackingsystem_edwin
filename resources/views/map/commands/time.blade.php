@extends('layouts.commandterminal')

@section('formfields')
<div class="form-group {{ $errors->has('offset') ? ' has-error' : '' }}">
    {{ Form::label('offset', 'Time offset') }}
    {{ Form::number('offset', 3, array('class' => 'form-control', 'min' => '-12', 'max' => '12', 'step' => '0.5', 'required', 'placeholder' => 'e.g 3 for +3:00 or -3 for -3:00')) }}
    @if ($errors->has('offset'))
        <span class="help-block">
            <strong>{{ $errors->first('offset') }}</strong>
        </span>
    @endif
</div>
@endsection