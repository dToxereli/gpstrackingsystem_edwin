<div>
    <div class="table-responsive">
        <table class="table table-striped data-table-control display responsive" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Terminal</th>
                    <th>Description</th>
                    <th>Identifier</th>
                    <th>IMEI</th>
                    <th>Group</th>
                    <th>Owner</th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
                <tr>                    
                    <th>Terminal</th>
                    <th>Description</th>
                    <th>Identifier</th>
                    <th>IMEI</th>
                    <th>Group</th>
                    <th>Owner</th>
                    <th></th>                 
                </tr>
            </tfoot>
            <tbody>
                @foreach ($terminals as $terminal)
                <tr class="terminal-entry" data-imei="{{ $terminal->imei }}">                        
                    
                    <td id="{{ $terminal->imei }}-name" data-name="{{ $terminal->display }}">
                        <span id="{{ $terminal->imei }}-status" class="offline">
                            <i class=" online-marker glyphicon glyphicon-signal"></i>
                        </span>
                        &nbsp;
                        {{ $terminal->display }}
                    </td>
                    <td>{{ ($terminal->description) ? $terminal->description : 'No description'}}</td>
                    <td>{{ ($terminal->identifier) ? $terminal->identifier : 'No Identifier'}}</td>
                    <td>{{ ($terminal->imei) ? $terminal->imei : 'No IMEI'}}</td>
                    <td>{{ ($terminal->terminal_group) ? $terminal->terminal_group->name : 'Ungrouped'}}</td>
                    <td>{{ ($terminal->user) ? $terminal->user->username : 'Not assigned' }}</td>                        
                    <td></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<script>
    $('.data-table-control').DataTable( {
        order: [[4, 'asc']],
        rowGroup: {
            dataSrc: 4
        },
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   -1
        } ],        
        select: true
    } );
</script>