@extends('layouts.controlpanel')

@section('terminallist')
    <div>
        <div class="">
            <table class="table table-responsive data-table-control display responsive" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Terminal</th>
                        <th></th>
                        <th>Description</th>
                        <th>Identifier</th>
                        <th>IMEI</th>
                        <th>Group</th>
                        <th>Owner</th>
                        <th></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>                    
                        <th>Terminal</th>
                        <th></th>
                        <th>Description</th>
                        <th>Identifier</th>
                        <th>IMEI</th>
                        <th>Group</th>
                        <th>Owner</th>                        
                        <th></th>                 
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($terminals as $terminal)
                    <tr class="terminal-entry" data-imei="{{ $terminal->imei }}">                        
                        
                        <td id="{{ $terminal->imei }}-name" data-name="{{ $terminal->display }}">
                            <span id="{{ $terminal->imei }}-status" class="offline">
                                <i class=" online-marker glyphicon glyphicon-signal"></i>
                            </span>
                            &nbsp;
                            <span>
                                {{ $terminal->display }}
                            </span>
                        </td>
                        <td>
                            <span>
                                <button class="btn" style="background-color: transparent; margin: 0; padding: 0; text-align: center"
                                    onclick="event.preventDefault();
                                            findTerminal({{$terminal->imei}});">
                                    <span class="mdi mdi-magnify mdi-18px mdi-dark"></span>
                                    Find on map
                                </button>
                            </span>
                        </td>
                        <td>{{ ($terminal->description) ? $terminal->description : 'No description'}}</td>
                        <td>{{ ($terminal->identifier) ? $terminal->identifier : 'No Identifier'}}</td>
                        <td>{{ ($terminal->imei) ? $terminal->imei : 'No IMEI'}}</td>
                        <td>{{ ($terminal->terminal_group) ? $terminal->terminal_group->name : 'Ungrouped'}}</td>
                        <td>{{ ($terminal->user) ? $terminal->user->username : 'Not assigned' }}</td>                        
                        <td></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('display')
    <div id="map"></div>
@endsection

@section('footer')
<script>
    $('#right-sidebar').hide();
    $('.partial-button.contained-right').each(function (index, elem) {  
        $(this).click(function (e) {
            $('#right-sidebar').show();
        });
    });
    var icons = {
        GREEN: "{{ asset('img/green-marker.png') }}",
        RED: "{{ asset('img/red-marker.png') }}",
        BLUE: "{{ asset('img/blue-marker.png') }}",
        PURPLE: "{{ asset('img/purple-marker.png') }}",
        BROWN: "{{ asset('img/brown-marker.png') }}",
        DARK: "{{ asset('img/dark-marker.png') }}",
        WHITE: "{{ asset('img/white-marker.png') }}"
    };
    var sockserverip = "{!! $socket['ip']; !!}";
    var sockserverport = Number("{!! $socket['port']; !!}");
</script>
<script type="text/javascript" src= "{{ asset('js/app/terminal.js') }}"></script>
<script>
    $('.data-table-control').DataTable( {
        order: [[5, 'asc']],
        rowGroup: {
            dataSrc: 5
        },
        responsive: {
            details: {
                type: 'column',
                target: -1
            },
            breakpoints: [
                { 
                    name: 'screen-xs',
                    width: 600 
                }
            ]
        },
        columnDefs: [ 
            {
                className: 'control',
                orderable: false,
                targets:   -1
            },
            { 
                className: 'screen-xs',
                targets: [1,2,3,4,5,6] 
            }
        ],        
        select: true
    } );    
    $('.partial-container').each(function(index){
        $(this).partialLoader('container');
    });
    $('.auto-load').each(function(index) {        
        $(this).partialLoader('autoload');
    });

    var loc = { lat: null, lng: null };
    @foreach ($terminals as $terminal)
        var data = {!! json_encode($terminal->only('imei','display','lastLocation','driver')) !!}
        var jsterminal = new Terminal(data);
        terminalList[data['imei']] = jsterminal;
    @endforeach
</script>
<script type="text/javascript" src= "{{ asset('js/app/map.js') }}"></script>
<script>
    $('#find').click(function (e) { 
        e.preventDefault();
        var imei = null;
        $('.terminal-entry.selected').each(function (index, element) {
            imei = $(this).data('imei');
        });
        findTerminal(imei);
    });
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={!! $apikey !!}&libraries=visualization&callback=initMap"></script>
<script type="text/javascript" src= "{{ asset('js/app/socketclient.js') }}"></script>
<style>
    .row div {
        width: 100%;
        padding: 0;
        margin: 0;
    }

    .group td {
        background-color: #ccd;
        color: #000;
    }

    table {
        background-color: #eee;
        color: #000;
    }

    .online-marker {
        padding: 0;
        margin: 0;
        width: 5%;
    }

    .table-responsive {
        max-height: 42em;
        position: relative;
        overflow: scroll;
        padding-left: 0;
        margin: 0;
    }
</style>
@endsection