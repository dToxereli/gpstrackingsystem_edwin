@extends('layouts.test')

@section('content')
    <p id="power">0</p>
@endsection

@section('footer')
<script type="text/javascript" src= "{{ asset('socket.io/socket.io.js') }}"></script>
    <script>
        var socket = io('http://localhost:3000');
        // var socket = io('http://192.168.0.148:3000');
        socket.on("prepare-monitor", function(message){
            // increase the power everytime we load test route
            $('#power').text(parseInt($('#power').text()) + parseInt(message.data.power));
        });
    </script>
@endsection