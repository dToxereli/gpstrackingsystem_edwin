@extends('layouts.partial')

@section('header')
    <h2>Add terminal</h2>
    <p>Add a new terminal to the system</p>
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')
    {{ Form::open(array('url' => 'terminals', 'class' => 'partial-form-embed', 'data-container' => 'terminaladmin')) }}

    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
        {{ Form::label('phone', 'Phone') }}
        {{ Form::text('phone', '', array('class' => 'form-control', 'required')) }}
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('imei') ? ' has-error' : '' }}">
        {{ Form::label('imei', 'IMEI') }}
        {{ Form::text('imei', '', array('class' => 'form-control', 'required')) }}
        @if ($errors->has('imei'))
            <span class="help-block">
                <strong>{{ $errors->first('imei') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('expiry_date') ? ' has-error' : '' }}">
        {{ Form::label('expiry_date', 'Expiry Date') }}
        {{ Form::date('expiry_date', '', array('class' => 'form-control', 'required')) }}
        @if ($errors->has('expiry_date'))
            <span class="help-block">
                <strong>{{ $errors->first('expiry_date') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('model') ? ' has-error' : '' }}">
        {{ Form::label('model', 'Model') }}
        {{ Form::text('model', 'TK 103A', array('class' => 'form-control', 'required')) }}
        @if ($errors->has('model'))
            <span class="help-block">
                <strong>{{ $errors->first('model') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('configuration') ? ' has-error' : '' }}">
        <h5><b>Choose configuration</b></h5>        
            {{ Form::radio('configuration',-1, true, ['class' => 'field']) }}
            {{ Form::label('configuration', 'None') }}
        @foreach ($configurations as $configuration)
            {{ Form::radio('configuration', $configuration->id, false, ['class' => 'field']) }}
            {{ Form::label('configuration', $configuration->configuration_name) }}
        @endforeach

        @if ($errors->has('configuration'))
            <span class="help-block">
                <strong>{{ $errors->first('configuration') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('identifier') ? ' has-error' : '' }}">
        {{ Form::label('identifier', 'Identifier') }} (Optional)
        {{ Form::text('identifier', '', array('class' => 'form-control')) }}
        @if ($errors->has('identifier'))
            <span class="help-block">
                <strong>{{ $errors->first('identifier') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
        {{ Form::label('description', 'Description') }} (Optional)
        {{ Form::text('description', '', array('class' => 'form-control')) }}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif        
    </div>

    <div class="form-group">
        <button data-source="{{ route('terminals.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="terminaladmin" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}
@endsection