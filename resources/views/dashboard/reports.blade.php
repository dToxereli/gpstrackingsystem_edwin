@extends('layouts.partial')

@section('header')
<div class="navbar partial-navbar">
    <div class="container-fluid">
      <div class="navbar-header">
        <span class="navbar-brand">Reports</span>
      </div>
      <ul class="nav navbar-nav partial-nav">
        <li class="active"><a class="partial-link-embed" data-container="reports" href="{{ url('terminalreports') }}">Terminal stats</a></li>
        <li><a class="partial-link-embed" data-container="reports" href="#">Terminal events</a></li>
        <li><a class="partial-link-embed" data-container="reports" href="#">Driver stats</a></li>
        <li><a class="partial-link-embed" data-container="reports" href="#">Driver events</a></li>
      </ul>
    </div>
</div>
@endsection

@section('body')
<div class="partial-container" id="reports" data-name="reports">
    <div class="auto-load-embed" data-source="{{ url('terminalreports') }}" data-container="reports" data-state="ready"></div>
</div>
@endsection

@section('footer')

@yield('footer')
@include('components.flash')

<script>            
    $(init)
    function init() {
        $('.partial-navbar a').each(function (index, element) {
            var element = this;
            $(element).on('click',function (e) {
                $('.partial-nav li.active').removeClass('active');
                $(element).parent().addClass('active');
            });
        });
        $('.partial-container').each(function(index){
            $(this).partialLoader('container');
        });
    }
</script>

@endsection