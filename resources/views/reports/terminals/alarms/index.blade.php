<div>
    
    @component('components.card')
    
        @slot('heading')
            <div class="row">
                {{$title}}
                <div class="pull-right">
                    <button id="remove-{{$id}}" data-target="#{{$id}}" class="btn btn-red pull-left partial-remove-embed" style="margin-right: 3px;">
                        <i class="mdi mdi-close mdi-18px"></i> Remove report
                    </button>
                    <button id="remove-{{$id}}" data-target="#{{$id}}" class="btn btn-green pull-left partial-remove-embed" style="margin-right: 3px;">
                        <i class="mdi mdi-file-excel mdi-18px"></i> Download Excel
                    </button>
                </div>
            </div>
        @endslot
    
    
        <div class="row">
            <div class="col-lg-6">
                    <div id="{{$id}}-pie"></div>
            </div>
            <div class="col-lg-6">
                    <div id="{{$id}}-donut"></div>
            </div>
        </div>
        <div class="row">
            <div id="{{$id}}-chart"></div>
        </div>
        <div class="row">
            <div id="{{$id}}-geo"></div></div>
        </div>
        <div class="row">
            <div id="{{$id}}-table"></div></div>
        </div>
        <div class="row">
            Table and summary
        </div>
    
    
        @slot('footer')
            {!! Lava::render($charttype, $id, $id.'-chart') !!}
            {!! Lava::render('PieChart', $id, $id.'-pie') !!}
            {!! Lava::render('DonutChart', $id, $id.'-donut') !!}
            <script>
                $('#remove-{{$id}}').partialLoader('remove');
            </script>
        @endslot
    
    @endcomponent
    
    </div>