{{ Form::open(array('url' => 'distancereports','method', 'id' => 'form1', 'class' => 'partial-form-embed', 'data-container' => $id)) }}

<input type="hidden" id="reportid" name="reportid" value="{{$id}}">

<div class="form-group {{ $errors->has('data') ? ' has-error' : '' }}">
    {{ Form::label('description', 'Select data') }}
    @if ($errors->has('data'))
        <span class="help-block">
            <strong>{{ $errors->first('data') }}</strong>
        </span>
    @endif
    
    <select id = "format" name="format">
        <option value = "0">Column chart</option>
        <option value = "1">Green</option>
        <option value = "2">Blue</option>
        <option value = "3">Cyan</option>
        <option value = "4">Magenta</option>
        <option value = "5">Yellow</option>
        <option value = "6">Black</option>
        <option value = "7">White</option>
    </select>

<div class="form-group">
    <button id="remove-{{$id}}" data-target="#{{$id}}" class="btn btn-red pull-left partial-remove-embed" style="margin-right: 3px;">
        <i class="mdi mdi-close mdi-18px"></i> Cancel
    </button>

    <button type="submit" class="btn btn-aqua">
        <i class="mdi mdi-arrow-right mdi-18px"></i> Next
    </button>
</div>

{{ Form::close() }}

</div>

<script>
    $('#remove-{{$id}}').partialLoader('remove');
    $('#form1').partialLoader('form');
</script>