@extends('layouts.partial')

{{-- @section('header')
    <h2>Server logs</h2>
    <p>View or download server logs</p>
@endsection --}}

{{-- @section('body')
    <div>
        {{ Form::open(array('url' => 'serverlogs/show', 'class' => 'partial-form-embed form-inline', 'data-container' => 'home')) }}

        <div class="form-group {{ $errors->has('logfile') ? ' has-error' : '' }}">
            {{ Form::label('logfile', 'Select a tog file to view') }}
            {{ Form::select('logfile', [
                'Web server logs', 'Socket server error log', 'Socket server output log'
            ], null, ['class' => 'form-control']) }}
            @if ($errors->has('logfile'))
                <span class="help-block">
                    <strong>{{ $errors->first('logfile') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('length') ? ' has-error' : '' }}">
            {{ Form::label('length', 'Number of lines') }}
            {{ Form::number('length', '10', array('class' => 'form-control', 'min' => '10', 'max' => '10000')) }}
            @if ($errors->has('length'))
                <span class="help-block">
                    <strong>{{ $errors->first('length') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-aqua">
                <i class="mdi mdi-upload mdi-18px"></i> View log
            </button>
        </div>
        {{ Form::close() }}
    </div>
    
    @if (isset($logdata))
        <div>
            <div>
                @component('components.card')
                    @slot('heading')
                        <div class="row">
                            <h4 class="text-center">Error log records</h4>
                        </div>
                    @endslot

                    <div>
                        <div id="logdisplay">{!!$logdata!!}</div>
                    </div>

                    @slot('footer')
                        <div style="min-height: 3em;" class="center-block">
                            <div>
                                <p>
                                    <strong>Size</strong>: {{$size}} bytes
                                </p>
                            </div>
                            <div>
                                <a class="btn btn-green" id="downloadLog" style="margin-right: 3px;">
                                    <i class="mdi mdi-file-document mdi-18px"></i> Download Log file
                                </a>
                            </div>
                        </div>
                    @endslot
                @endcomponent
            </div>
        </div>
    @endif

@endsection --}}

{{-- @section('footer')
    <script>
        function downloadInnerHtml(filename, elId) {
            var elHtml = document.getElementById(elId).innerHTML;
            elHtml = elHtml.replace(/<br\s*[\/]?>/gi,'\n');
            var link = document.createElement('a');
            mimeType = 'text/plain';
            link.setAttribute('download', filename);
            link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(elHtml));
            document.body.append(link)
            link.click(); 
            document.body.removeChild(link)
        }

        $(init)

        function init() {
            $('#downloadLog').click(function(e){
                downloadInnerHtml('log', 'logdisplay');
            });
        }
    </script>
@endsection --}}

@section('body')
    <div class="row" style="margin-top: 1em; padding: 0">
        <iframe src="{{route('log-viewer::dashboard')}}" frameborder="0" style="height: 42em; width: 100%">
        </iframe>
    </div>
@endsection