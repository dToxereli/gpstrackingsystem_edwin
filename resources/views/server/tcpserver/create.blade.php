@extends('layouts.partial')

@section('header')
    <h2>Create TCP server</h2>    
    <p>Create a new TCP server</p>    
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
    <p class="text-warning">This configuration will momentarily reset the server. All terminals will lose connection to the server for up to a few minutes</p>
@endsection

@section('body')

    {{ Form::open(array('url' => 'tcpservers', 'class' => 'partial-form-embed', 'data-container' => 'home')) }}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', '', array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('ip') ? ' has-error' : '' }}">
        {{ Form::label('ip', 'IP Address') }}
        {{ Form::text('ip', '', array('class' => 'form-control')) }}
        @if ($errors->has('ip'))
            <span class="help-block">
                <strong>{{ $errors->first('ip') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('port') ? ' has-error' : '' }}">
        {{ Form::label('port', 'Port') }}
        {{ Form::text('port', '', array('class' => 'form-control')) }}
        @if ($errors->has('port'))
            <span class="help-block">
                <strong>{{ $errors->first('port') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <button data-source="{{ route('tcpservers.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}
@endsection