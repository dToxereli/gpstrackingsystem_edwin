@extends('layouts.partial')

@section('header')
    <h2>Edit API Key</h2>    
    <p>Modify an existing API Key</p>    
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
    <p class="text-warning">This configuration will momentarily reset the server. All terminals will lose connection to the server for up to a few minutes</p>
@endsection

@section('body')

{{ Form::open(array('url' => route('apikeys.update', $socket['name']), 'class' => 'partial-form-embed', 'data-container' => 'home')) }}
    <input name="_method" value="PUT" type="hidden">
    
    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', $socket['name'], array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('key') ? ' has-error' : '' }}">
        {{ Form::label('key', 'Key') }}
        {{ Form::password('key', array('class' => 'form-control')) }}
        @if ($errors->has('key'))
            <span class="help-block">
                <strong>{{ $errors->first('key') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <button data-source="{{ route('apikeys.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}
@endsection