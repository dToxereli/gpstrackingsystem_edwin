@extends('layouts.partial')

@section('header')
    <h2>Manage API keys</h2>
    <p>Create, edit and remove API keys from the system for serving the live map</p>
    <p class="text-warning">Be careful when changing server settings. Carelessness may kill your application or your server</p>
@endsection

@section('body')
    <div class="table-responsive">
        <table class="table table-responsive data-table-embed" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Operation</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($keys as $key)
                <tr>
                    <td>{{ $key['name'] }}</td> 
                    <td>
                    <a href="{{ URL::to('apikeys/'.$key['name'].'/edit') }}" class="btn btn-blue pull-left partial-link-embed" data-container="home" style="margin-right: 3px;">Edit</a>
    
                    {!! Form::open(['method' => 'DELETE', 'route' => ['apikeys.destroy', $key['name']], 'class' => 'partial-form-embed', 'data-container' => 'home' ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-red']) !!}
                    {!! Form::close() !!}
    
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ URL::to('apikeys/create') }}" class="btn btn-green partial-link-embed" data-container="home">Add API key</a>
    </div>    
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection