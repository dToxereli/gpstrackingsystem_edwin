{{-- \resources\views\errors\401.blade.php --}}
@extends('layouts.app')

@section('content')
    <div class='text-center tada animated'>
        <h1>404</h1>        
        <h1>PAGE NOT FOUND</h1>
        <p>The page you were looking for could not be found</p>
    </div>
@endsection