@extends('layouts.app')

@section('content')
    <div class="text-center tada animated">
        <h1>403</h1>
        <h1>FORBIDDEN</h1>
        <h4 class="text-danger"> You are not allowed to access this page</h4>
    </div>
@endsection