@extends('layouts.controlpanelalt')

@section('terminallist')
    <div>
        <div class="table-responsive col-sm-11">
            <table class="table table-bordered table-striped data-table-control" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Terminal</th>
                        <th>Description</th>
                        <th>Identifier</th>
                        <th>IMEI</th>
                        <th>Group</th>
                        <th>Owner</th>
                        <th></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>                    
                        <th>Terminal</th>
                        <th>Description</th>
                        <th>Identifier</th>
                        <th>IMEI</th>
                        <th>Group</th>
                        <th>Owner</th>
                        <th></th>                 
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($terminals as $terminal)
                    <tr class="terminal-entry" data-imei="{{ $terminal->imei }}">                        
                        
                        <td id="{{ $terminal->imei }}-name" data-name="{{ $terminal->display }}">
                            <span class=" online-marker glyphicon glyphicon-signal offline" id="{{ $terminal->imei }}-status"></span>
                            &nbsp;
                            {{ $terminal->display }}
                        </td>
                        <td>{{ ($terminal->description) ? $terminal->description : 'No description'}}</td>
                        <td>{{ ($terminal->identifier) ? $terminal->identifier : 'No Identifier'}}</td>
                        <td>{{ ($terminal->imei) ? $terminal->imei : 'No IMEI'}}</td>
                        <td>{{ ($terminal->terminal_group) ? $terminal->terminal_group->name : 'Ungrouped'}}</td>
                        <td>{{ ($terminal->user) ? $terminal->user->username : 'Not assigned' }}</td>                        
                        <td></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('display')
<script>
    //var temp = {!! json_encode($terminals->toArray()) !!};
    //console.log(temp);
    var visterminals = {};
    var loc = { lat: null, lng: null };
    @foreach ($terminals as $terminal)
        var terminal = {!! json_encode($terminal) !!}
        loc = { lat: null, lng: null };
        @if(isset($terminal->lastLocation))
            loc = {
                lat: {{ $terminal->lastLocation->latitude }},
                lng: {{ $terminal->lastLocation->longitude }}
            };
        @endif
        terminal['lastLocation'] = loc;
        terminal['name'] = terminal['description'] || terminal['identifier'] || terminal['imei'];
        visterminals[terminal['imei']] = terminal;
    @endforeach
    console.log(visterminals);
</script>
<div class="partial-container" id="display" data-name="display">
    <div class="mapcontent-holder partial-container" id="right-sidebar" data-name="right-sidebar">
    </div>
    <div class="container-fluid" id="map">
    
    </div>
</div>
@endsection
    
@section('footer')
<script type="text/javascript" src= "{{ asset('js/app/socketmaster.js') }}"></script>
<script type="text/javascript" src= "{{ asset('js/app/mapmaster.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAH_JEW5yzDXLdpaXVLzx01rtvPu3j_8W4&callback=initMap"></script>
<script>
    $('.data-table-control').DataTable( {
        order: [[4, 'asc']],
        rowGroup: {
            dataSrc: 4
        },
        responsive: {
            details: {
                type: 'column',
                target: -1
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   -1
        } ],        
        select: true
    } );    
    $('.partial-container').each(function(index){
        $(this).partialLoader('container');
    });
    $('.auto-load').each(function(index) {        
        $(this).partialLoader('autoload');
    });
</script>

<style>
    .row div {
        width: 100%;
        padding: 0;
        margin: 0;
    }

    .group td {
        background-color: #ccd;
        color: #000;
        border-left: 2px solid #00f; 
    }

    .online-marker {
        padding: 0;
        margin: 0;
        width: 5%;
    }

    .table-responsive {
        max-height: 45em;
        overflow: scroll;
        padding-left: 0;
        margin: 0;
    }
</style>
@endsection