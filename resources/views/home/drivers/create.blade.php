@extends('layouts.partial')

@section('header')
    <h2>Add driver</h2>    
    <p>Add a new driver to the system</p>    
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')

{{ Form::open(array('url' => 'drivers', 'class' => 'partial-form-embed', 'data-container' => 'home')) }}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Driver Name') }}
        {{ Form::text('name', '', array('class' => 'form-control', 'required', 'placeholder' => 'Name')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', '', array('class' => 'form-control', 'required', 'placeholder' => 'driver@example.com')) }}
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
        {{ Form::label('phone', 'Phone') }}
        {{ Form::text('phone', '', array('class' => 'form-control', 'required', 'placeholder' => '+254712345689')) }}
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

    <h5><b>Assign to vehicle</b></h5>

    <div class='form-group'>
        <label class="switch">
            {{ Form::radio('vehicle',  -1, true, ['id' => -1] ) }}
            <span class="slider round"></span>
        </label>
        {{ Form::label(-1, 'None') }}<br>
        @foreach ($terminals as $terminal)
            <label class="switch">
                {{ Form::radio('vehicle',  $terminal->id, null, ['id' => $terminal->id] ) }}
                <span class="slider round"></span>
            </label>
            {{ Form::label($terminal->id, ucfirst($terminal->display)) }}<br>
        @endforeach
    </div>

    <div class="form-group">
        <button data-source="{{ route('drivers.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}    

@endsection