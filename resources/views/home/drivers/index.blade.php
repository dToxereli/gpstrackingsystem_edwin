@extends('layouts.partial')

@section('header')
    <h2>Manage Drivers</h2>
    <p>Add, edit, remove and set drivers in the system</p>
@endsection

@section('body')
    <div class="table-responsive">    
        <table class="table data-table-embed" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Contacts</th>
                    <th>Current vehicle</th>
                    <th>Operations</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Contacts</th>
                    <th>Current vehicle</th>
                    <th>Operations</th>
                </tr>
            </tfoot>
    
            <tbody>
                @foreach ($drivers as $driver)
                <tr>
                    <td>{{ $driver->name }}</td>
                    <td>
                        <b>E-mail: </b>{{ $driver->email }}
                        <br>
                        <b>Phone: </b>{{ $driver->phone }}
                    </td>
                    <td>
                        {{ isset($driver->terminal) ? $driver->terminal->display : "Not driving" }}
                    </td>
                    <td>
                        <button type="button" data-source="{{ route('drivers.edit', $driver->id) }}" class="btn btn-blue pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">Edit</button>
        
                        {!! Form::open(['method' => 'DELETE', 'route' => ['drivers.destroy', $driver->id], 'class' => 'partial-form-embed', 'data-container' => 'home' ]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-red']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
    
        </table>
        <button type="button" id="btn-adddriver" data-source="{{ route('drivers.create') }}" class="btn btn-green partial-button-embed" data-container="home">Add Driver</button>
    </div>    
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection
