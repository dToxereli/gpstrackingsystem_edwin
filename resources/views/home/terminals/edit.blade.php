@extends('layouts.partial')

@section('header')
    
@endsection

@section('body')
    {{ Form::model($terminal, array('route' => array('userterminals.update', $terminal->id), 'method' => 'PUT', 'class' => 'form-horizontal partial-form-embed', 'data-container' => 'home')) }}       
    <div class="form-group {{ $errors->has('identifier') ? ' has-error' : '' }}">
        {{ Form::label('identifier', 'Identifier') }}
        {{ Form::text('identifier', $terminal->identifier, array('class' => 'form-control', 'placeholder' => 'New identifier')) }}
        @if ($errors->has('identifier'))
            <span class="help-block">
                <strong>{{ $errors->first('identifier') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
        {{ Form::label('description', 'Description', ['class' => 'control-label']) }}
        {{ Form::text('description', $terminal->description, array('class' => 'form-control', 'placeholder' => 'New description')) }}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
    
    <div class="form-group {{ $errors->has('terminal_group') ? ' has-error' : '' }}">
        <h5><b>Choose terminal group</b></h5>      
            <div>
                <label class="switch">
                        {{ Form::radio('terminalgroup',-1, $terminal->terminal_group == null, ['class' => 'field']) }}
                    <span class="slider round"></span>
                </label>
                {{ Form::label('terminalgroup', 'None') }}
            </div>
        @foreach ($terminal_groups as $terminal_group)
            <div>
                <label class="switch">
                    {{ Form::radio('terminalgroup', $terminal_group->id, ($terminal->terminal_group == null) ? false : $terminal_group->id == $terminal->terminal_group->id, ['class' => 'field']) }}
                    <span class="slider round"></span>
                </label>
                {{ Form::label('terminalgroup', $terminal_group->name) }}
            </div>
        @endforeach

        @if ($errors->has('terminal_group'))
            <span class="help-block">
                <strong>{{ $errors->first('terminal_group') }}</strong>
            </span>
        @endif
    </div>    

    <div class="form-group">
        <button data-source="{{ route('userterminals.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}
@endsection

@section('footer')
    
@endsection