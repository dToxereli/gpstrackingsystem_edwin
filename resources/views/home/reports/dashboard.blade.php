@extends('layouts.partial')

@section('body')
    <br><br>
    <div class="row">
        <div class="col-md-3">
            <a href="{{ route('terminalperformance.create') }}" class="btn btn-primary btn-fullwidth pull-left report-btn" data-container="reportscontainer" data-mode="prepend" style="margin-right: 3px;">
                <div>
                    <span class="mdi mdi-car mdi-48px"></span>
                    <span class="mdi mdi-chart-areaspline mdi-36px"></span>
                </div>
                <div>
                    <span class="mdi mdi-plus-circle-outline mdi-18px"></span>
                    Terminal performance report
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="{{ route('terminalalarms.create') }}" class="btn btn-primary btn-fullwidth pull-left report-btn" data-container="reportscontainer" data-mode="prepend" style="margin-right: 3px;">
                
                <div>
                    <span class="mdi mdi-car mdi-48px"></span>
                    <span class="mdi mdi-alert-outline mdi-36px"></span>
                </div>
                <div>
                    <span class="mdi mdi-plus-circle-outline mdi-18px"></span>
                    Terminal alarm report
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="{{ route('driverperformance.create') }}" class="btn btn-primary btn-fullwidth pull-left report-btn" data-container="reportscontainer" data-mode="prepend" style="margin-right: 3px;">
                
                <div>
                    <span class="mdi mdi-steering mdi-48px"></span>
                    <span class="mdi mdi-chart-areaspline mdi-36px"></span>
                </div>
                <div>
                    <span class="mdi mdi-plus-circle-outline mdi-18px"></span>
                    Driver performance report
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="{{ route('driveralarms.create') }}" class="btn btn-primary btn-fullwidth pull-left report-btn" data-container="reportscontainer" data-mode="prepend" style="margin-right: 3px;">
                
                <div>
                    <span class="mdi mdi-steering mdi-48px"></span>
                    <span class="mdi mdi-alert-outline mdi-36px"></span>
                </div>
                <div>
                    <span class="mdi mdi-plus-circle-outline mdi-18px"></span>
                    Driver alarm report
                </div>
            </a>
        </div>

    </div>

    <div class="partial-container" id="reportscontainer" data-name="reportscontainer"></div>
@endsection

@section('footer')
    <script>
        $('.report-btn').partialLoader('link');
    </script>
@endsection