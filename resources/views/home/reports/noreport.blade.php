<div>
    
    @component('components.card')

        @slot('heading')
            <div class="row">
                <div class="col-sm-8">
                    <strong>{{$title}}</strong>
                </div>
                <div class="pull-right col-sm-2">
                    <button id="remove-{{$id}}" data-target="#{{$id}}" class="btn btn-red pull-left partial-remove-embed" style="margin-right: 3px;">
                        <i class="mdi mdi-close mdi-18px"></i> Remove section
                    </button>
                </div>
            </div>
        @endslot
    
        <div class="row">
            <div class="col-lg-6">
                <h3 class="text-danger">No data</h3>
                <h4>
                    There is no data available for the selected inputs. Click on new report to create
                    a new report.
                </h4>
            </div>
        </div>
    
        @slot('footer')
            
            <script>
                $('#remove-{{$id}}').partialLoader('remove');
            </script>
        @endslot
    
    @endcomponent
    
</div>