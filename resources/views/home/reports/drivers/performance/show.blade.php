<div class="report">
    
    @component('components.card')

        @slot('heading')
            <div class="row">
                <strong>{{$title}}</strong>
            </div>
        @endslot
    
        <div class="row">
            <div class="col-lg-6">
                <h6 class="text-center">Time based summary chart</h6>
                <div id="{{$id}}-pie"></div>
            </div>
            <div class="col-lg-6">
                <h6 class="text-center">Driver based summary chart</h6>
                <div id="{{$id}}-donut"></div>
            </div>
        </div>
        <div class="row">
                <h5 class="text-center">Trend chart</h5>
            <div id="{{$id}}-chart"></div>
        </div>
    
        @slot('footer')
            <div class="row center-block">
                <div>
                    <button id="remove-{{$id}}" data-target="#{{$id}}" class="btn btn-red pull-left partial-remove-embed" style="margin-right: 3px;">
                        <i class="mdi mdi-close mdi-18px"></i> Remove report
                    </button>
                </div>
                <div>
                    {{ Form::open(array('url' => route('exportreport'), 'method' => 'POST', 'class' => 'form-inline', 'target' => "_blank")) }}

                    {{ Form::hidden('data', json_encode($data), array('required')) }}
                    {{ Form::hidden('title', $title, array('required')) }}

                    <button type="submit" class="btn btn-green pull-left" style="margin-right: 3px;">
                        <i class="mdi mdi-file-excel mdi-18px"></i> Download Excel
                    </button>

                    {{ Form::close() }}
                </div>
            </div>
            {!! Lava::render($charttype, $id, $id.'-chart') !!}
            {!! Lava::render('PieChart', $id, $id.'-pie') !!}
            {!! Lava::render('DonutChart', $id, $id.'-donut') !!}
            <script>
                $('#remove-{{$id}}').partialLoader('remove');
            </script>
        @endslot
    
    @endcomponent
    
</div>