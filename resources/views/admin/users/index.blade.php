@extends('layouts.partial')

@section('header')
    <h2>Manage Users</h2>
    <p>Add, edit, remove and set user groups for users in the system</p>
@endsection

@section('body')
    <div class="table-responsive">    
        <table class="table data-table-embed" width="100%">
            <colgroup>
                <col width="15%">
                <col width="30%">
                <col width="20%">
                <col width="15%">
                <col width="20%">
            </colgroup>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Contacts</th>
                    <th>Date/Time Added</th>
                    <th>Group memberships</th>
                    <th>Operations</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Contacts</th>
                    <th>Date/Time Added</th>
                    <th>Group memberships</th>
                    <th>Operations</th>
                </tr>
            </tfoot>
    
            <tbody>
                @foreach ($users as $user)
                <tr>
    
                    <td>{{ $user->username }}</td>
                    <td>
                        <b>E-mail: </b>{{ $user->email }}
                        <br>
                        <b>Phone: </b>{{ $user->phone }}
                    </td>
                    <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                    <td>
                        <ul>
                            @foreach ($user->roles as $role)
                                <li>{{ $role->name }}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                    <button type="button" data-source="{{ route('users.edit', $user->id) }}" class="btn btn-blue pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">Edit</button>
    
                    {!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id], 'class' => 'partial-form-embed', 'data-container' => 'home' ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-red']) !!}
                    {!! Form::close() !!}
    
                    </td>
                </tr>
                @endforeach
            </tbody>
    
        </table>
        <button type="button" id="btn-adduser" data-source="{{ route('users.create') }}" class="btn btn-green partial-button-embed" data-container="home">Add User</button>
    </div>    
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection
