@extends('layouts.partial')

@section('header')
    <h2>Create user group</h2>    
    <p>Create a new user group</p>    
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')
    
    {{ Form::open(array('url' => 'roles', 'class' => 'form-horizontal partial-form-embed', 'data-container' => 'home')) }}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <h5><b>Assign Permissions</b></h5>

    <div class="form-group {{ $errors->has('permissions') ? ' has-error' : '' }}">
        @foreach ($permissions as $permission)
            <label class="switch">
                {{ Form::checkbox('permissions[]',  $permission->id ) }}
                <span class="slider round"></span>
            </label>
            {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>
        @endforeach
        @if ($errors->has('permissions'))
            <span class="help-block">
                <strong>{{ $errors->first('permissions') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <button data-source="{{ route('roles.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}

@endsection