@extends('layouts.partial')

@section('header')
    <h2>Manage User Groups</h2>
    <p>Create, edit, remove and set user permissions for each user group in the system</p>
@endsection

@section('body')
    <div class="table-responsive">
        <table class="table data-table-embed" width="100%">
            <thead>
                <tr>
                    <th>Group</th>
                    <th>Permissions</th>
                    <th>Operation</th>
                </tr>
            </thead>
    
            <tbody>
                @foreach ($roles as $role)
                <tr>
    
                    <td>{{ $role->name }}</td>
    
                    <td>
                        <ul>
                            @foreach ($role->permissions as $permission)
                                <li>{{ $permission->name }}</li>
                            @endforeach
                        </ul>
                    <td>
                    <button type="button" data-source="{{ URL::to('roles/'.$role->id.'/edit') }}" class="btn btn-blue pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">Edit</button>
    
                    {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id], 'class' => 'partial-form-embed', 'data-container' => 'home' ]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-red']) !!}
                    {!! Form::close() !!}
    
                    </td>
                </tr>
                @endforeach
            </tbody>
    
        </table>
        
        <button type="button" data-source="{{ URL::to('roles/create') }}" class="btn btn-green partial-button-embed" data-container="home">Add Group</button>
    </div>    
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection