@extends('layouts.partial')

@section('header')
    <h2>Add terminal</h2>
    <p>Add a new terminal to the system</p>
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')
    
@endsection

@section('footer')
    
@endsection