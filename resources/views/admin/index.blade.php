@extends('layouts.manage')

@section('side-bar')

<li class="active"><a class="partial-link" data-container="admin" href="{{ route('admin.dashboard') }}">Dashboard</a></li>
@can('Manage Users')
    <li><a class="partial-link" data-container="admin" href="{{ route('users.index') }}"><span class="mdi mdi-account mdi-18px"></span> Users</a></li>
@endcan
@can('Administer roles and permissions')
    <li><a class="partial-link" data-container="admin" href="{{ route('roles.index') }}"><span class="mdi mdi-account-multiple mdi-18px"></span> Groups</a></li>
    <li><a class="partial-link" data-container="admin" href="{{ route('permissions.index') }}"><span class="mdi mdi-key-variant mdi-18px"></span> Permissions</a></li>
@endcan

@can('Manage Terminals - Admin')
    <li><a class="partial-link" data-container="admin" href="{{ route('terminals.index') }}"><span class="mdi mdi-car mdi-18px"></span> Terminals</a></li>
    <li><a class="partial-link" data-container="admin" href="{{ route('terminalconfigs.index') }}"><span class="mdi mdi-wrench mdi-18px"></span> Configs</a></li>
@endcan

@endsection

@section('body')    
<div class="partial-container" id="admin" data-name="admin">
    <div class="auto-load" data-source="{{ route('admin.dashboard') }}" data-container="admin" data-state="ready"></div>
</div>
@endsection

@section('footer')
    <script>
        $('.partial-container').each(function(index){
            $(this).partialLoader('container');
        });
        $('.partial-link').each(function(index){            
            $(this).partialLoader('link');
        });
        $('.auto-load').each(function(index) {        
            $(this).partialLoader('autoload');
        });
    </script>
@endsection