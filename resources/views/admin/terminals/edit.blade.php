@extends('layouts.partial')

@section('header')
    <h2>Edit terminal</h2>
    <h4>{{ $terminal->display }}</h4>
    <p>Edit terminal details</p>
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')
    {{ Form::model($terminal, array('route' => array('terminals.update', $terminal->id), 'method' => 'PUT', 'class' => 'partial-form-embed', 'data-container' => 'home')) }}    
    
    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
        {{ Form::label('phone', 'Phone') }}
        {{ Form::text('phone', $terminal->phone, array('class' => 'form-control')) }}
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('imei') ? ' has-error' : '' }}">
        {{ Form::label('imei', 'IMEI') }}
        {{ Form::text('imei', $terminal->imei, array('class' => 'form-control')) }}
        @if ($errors->has('imei'))
            <span class="help-block">
                <strong>{{ $errors->first('imei') }}</strong>
            </span>
        @endif
    </div>    

    <div class="form-group {{ $errors->has('expiry_date') ? ' has-error' : '' }}">
        {{ Form::label('expiry_date', 'Expiry Date') }}
        {{ Form::date('expiry_date', $terminal->expiry_date, array('class' => 'form-control')) }}
        @if ($errors->has('expiry_date'))
            <span class="help-block">
                <strong>{{ $errors->first('expiry_date') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('model') ? ' has-error' : '' }}">
        {{ Form::label('model', 'Model') }}
        {{ Form::text('model', $terminal->model, array('class' => 'form-control')) }}
        @if ($errors->has('model'))
            <span class="help-block">
                <strong>{{ $errors->first('model') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group {{ $errors->has('configuration') ? ' has-error' : '' }}">
        <h5><b>Choose configuration</b></h5>  
            <label class="switch">
                {{ Form::radio('configuration',-1, $terminal->configuration == null, ['class' => 'field']) }}
                <span class="slider round"></span>
            </label>      
            {{ Form::label('configuration', 'None') }}
        @foreach ($configurations as $configuration)
            <label class="switch">
                {{ Form::radio('configuration', $configuration->id, ($terminal->configuration == null) ? false : $configuration->id == $terminal->configuration->id, ['class' => 'field']) }}
                <span class="slider round"></span>
            </label>      
            {{ Form::label('configuration', $configuration->configuration_name) }}
        @endforeach

        @if ($errors->has('configuration'))
            <span class="help-block">
                <strong>{{ $errors->first('configuration') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('identifier') ? ' has-error' : '' }}">
        {{ Form::label('identifier', 'Identifier') }}
        {{ Form::text('identifier', $terminal->identifier, array('class' => 'form-control', 'placeholder' => 'New identifier')) }}
        @if ($errors->has('identifier'))
            <span class="help-block">
                <strong>{{ $errors->first('identifier') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
        {{ Form::label('description', 'Description') }}
        {{ Form::text('description', $terminal->description, array('class' => 'form-control', 'placeholder' => 'New description')) }}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <button data-source="{{ route('terminals.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="home" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}
@endsection