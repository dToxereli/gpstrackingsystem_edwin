@extends('layouts.partial')

@section('header')
    <h2>Manage terminal</h2>
    <p>Add, edit, remove, assign or configure terminals in the system</p>    
@endsection

@section('body')
    <div class="table-responsive">
        <table class="table data-table-embed" width="100%">
            <thead>
                <tr>
                    <th>Phone Number</th>
                    <th>IMEI</th>
                    <th>Date/Time added</th>
                    <th>Expiry Date</th>
                    <th>Model</th>
                    <th>Owner</th>
                    <th>Configuration</th>
                    <th>Operations</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Phone Number</th>
                    <th>IMEI</th>
                    <th>Date/Time added</th>
                    <th>Expiry Date</th>
                    <th>Model</th>
                    <th>Owner</th>
                    <th>Configuration</th>
                    <th>Operations</th>
                </tr>
            </tfoot>
    
            <tbody>
                @foreach ($terminals as $terminal)
                <tr>
                    <td>{{ $terminal->phone }}</td>
                    <td>{{ $terminal->imei }}</td>
                    <td>{{ $terminal->created_at->format('F d, Y h:ia') }}</td>
                    <td>{{ $terminal->expiry_date }}</td>
                    <td>{{ $terminal->model }}</td>
                    <td>{{ ($terminal->user) ? $terminal->user->username : 'Not assigned' }}</td>
                    <td>{{ ($terminal->configuration) ? $terminal->configuration->configuration_name : 'None'}}</td>
                    <td>
                        <button type="button" data-source="{{ route('terminals.editUser', $terminal->id) }}" class="btn btn-aqua btn-fullwidth partial-button-embed" data-container="home" style="margin-right: 3px;">Set Client</button>
                        <button type="button" data-source="{{ route('terminals.edit', $terminal->id) }}" class="btn btn-blue btn-fullwidth partial-button-embed" data-container="home" style="margin-right: 3px;">Edit</button>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['terminals.destroy', $terminal->id], 'class' => 'partial-form-embed', 'data-container' => 'home' ]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-red btn-fullwidth']) !!}
                        {!! Form::close() !!}                                                
                    </td>
                </tr>
                @endforeach
            </tbody>
    
        </table>
        <button type="button" id="btn-addterminal" data-source="{{ route('terminals.create') }}" class="btn btn-green partial-button-embed" data-container="home">Add terminal</button>
        <button type="button" id="btn-globalterminalconfig" data-source="" class="btn btn-red partial-button-embed" data-container="home">Global Terminal Configuration</button>
    </div>
@endsection

@section('footer')
    @include('components.embeddedtable')
@endsection