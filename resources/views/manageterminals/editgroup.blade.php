@extends('layouts.partial')

@section('header')
    <h2>Create terminal group</h2>    
    <p>Create a new terminal group</p>    
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')
    
@endsection

@section('footer')
    {{ Form::model($terminal_group, array('route' => array('terminalgroups.update', $terminal_group->id), 'method' => 'PUT', 'class' => 'partial-form-embed', 'data-container' => 'terminalmanagement')) }}       

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', $terminal_group->name, array('class' => 'form-control', 'placeholder' => 'New name')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <button data-source="{{ route('terminalgroups.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="terminalmanagement" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}
@endsection