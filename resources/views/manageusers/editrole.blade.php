@extends('layouts.partial')

@section('header')
    <h2>Edit user group</h2>
    <h4>{{ $role->name }}</h4>
    <p>Change user permission details</p>
    <p class="text-danger" ><strong>CAUTION!!</strong> Renaming some user groups could break the application</p>
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')
    {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT', 'class' => 'partial-form-embed', 'data-container' => 'admin')) }}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Role Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <h5><b>Assign Permissions</b></h5>

    <div class="form-group {{ $errors->has('permissions') ? ' has-error' : '' }}">
        @foreach ($permissions as $permission)
            {{Form::checkbox('permissions[]',  $permission->id, $role->permissions ) }}
            {{Form::label($permission->name, ucfirst($permission->name)) }}<br>
        @endforeach
        @if ($errors->has('permissions'))
            <span class="help-block">
                <strong>{{ $errors->first('permissions') }}</strong>
            </span>
        @endif
    </div>
    
    <div class="form-group">
        <button data-source="{{ route('roles.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="admin" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}    
@endsection