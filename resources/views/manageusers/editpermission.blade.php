@extends('layouts.partial')

@section('header')
    <h2>Edit permission</h2>
    <h4>{{ $permission->name }}</h4>
    <p>Change user permission details</p>
    <p class="text-danger" ><strong>CAUTION!!</strong> Renaming some user groups could break the application</p>
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif
@endsection

@section('body')
    {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT', 'class' => 'partial-form-embed', 'data-container' => 'admin')) }}{{-- Form model binding to automatically populate our fields with permission data --}}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Permission Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <button data-source="{{ route('permissions.index') }}" class="btn btn-red pull-left partial-button-embed" data-container="admin" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>
    
        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Submit
        </button>
    </div>

    {{ Form::close() }}    
@endsection