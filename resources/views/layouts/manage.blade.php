@extends('layouts.app')

@section('content')
<div class="wrapper">
    {{-- <div> --}}
        {{-- <button id="sidebar-toggle" type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#sidebar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button> --}}
    {{-- </div> --}}
    <div class="col-md-2 bg-dark full-height sidebar slideInLeft animated">
        <nav class='navbar manage-sidebar'>
            <div class="navbar-header pull-left">
                @section('brand')
                    <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                @endsection
            </div>
            <div class="collapse navbar-collapse" id="sidebar-collapse">
                <ul class="nav nav-stacked nav-sidebar">
                    @yield('side-bar')
                </ul>
                <hr class="ruler">
            </div>
        </nav>
    </div>

    <div class='col-md-10 full-height bg-transparent text-dark'>
        @yield('body')
    </div>

</div>

<script>            
    $(init)
    function init() {
        $('.nav-sidebar .btn-sidebarlink').each(function (index, element) {
            var element = this;
            $(element).on('click',function (e) {
                $('.nav-sidebar li.active').removeClass('active');
                $(element).parent().addClass('active');
            });
        }); 
    }
</script>
@yield('footer')
@endsection