@component('components.modalcontent')

@slot('title')
    Terminal command    
@endslot

    <h3>{{$description}}</h3>
    @if (count($errors) > 0)
        <p class="text-danger" >It seems there were errors in the input</p>
    @endif

    {{ Form::open(array('route' => ['commandterminal.send',$command], 'class' => 'form-horizontal partial-form-embed', 'data-container' => 'modal-content')) }}
    <div class="form-group {{ $errors->has('terminals') ? ' has-error' : '' }}">
        {{ Form::label('', 'Choose terminals to send this command to') }}

        <div class="table-responsive" style="margin:0px auto">
            <table class="table table-bordered">
                <colgroup>
                    <col span="1" style="width: 3%; background-color: grey">
                </colgroup>
                <thead>
                    <tr>
                        <th>Select</th>
                        <th>Terminal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($terminals as $terminal)
                        <tr>
                            <td>
                                <label class="switch">
                                    {{ Form::checkbox('terminals[]',  $terminal->id ) }}
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>{{ Form::label($terminal->display, ucfirst($terminal->display)) }}<br></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
                
        </div>

        @if ($errors->has('terminals'))
            <span class="help-block">
                <strong>{{ $errors->first('terminals') }}</strong>
            </span>
        @endif
    </div>

    @yield('formfields')

    <div class="form-group">
        <button data-dismiss="modal" id="close-modal" class="btn btn-red pull-left" style="margin-right: 3px;">
            <i class="mdi mdi-close mdi-18px"></i> Cancel
        </button>

        <button type="submit" class="btn btn-aqua">
            <i class="mdi mdi-upload mdi-18px"></i> Send command
        </button>
    </div>
    {{ Form::close() }}    

    <div>
        <script>
            $('.partial-link-embed').each(function(index){
                $(this).partialLoader('link');
            });
            $('.partial-form-embed').each(function(index) {        
                $(this).partialLoader('form');
            });
            $('.partial-button-embed').each(function(index) {        
                $(this).partialLoader('button');
            });
            $('.auto-load-embed').each(function(index) {        
                $(this).partialLoader('autoload');
            });        
        </script>
        @yield('footer')
    </div>
@endcomponent