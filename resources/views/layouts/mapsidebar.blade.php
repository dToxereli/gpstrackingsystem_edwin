<div class="slideInRight animated" id="sidebarcontent">
    @include('components.flash')
    <div>
        <button type="button" class="btn bg-transparent pull-right" id="close-sidebar" data-toggle="tooltip" title="Close" data-placement="bottom">
            <span class="mdi mdi-close-circle mdi-24px"></span>
        </button>
        <span class="h4-sim text-center">@yield('title')</span>
    </div>
    <div>
        @yield('body')
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#close-sidebar").tooltip();
        $("#close-sidebar").click(function(e) {
            $('#right-sidebar').hide();
        });
        $('.partial-link-embed').each(function(index){
            $(this).partialLoader('link');
        });
        $('.partial-form-embed').each(function(index) {        
            $(this).partialLoader('form');
        });
    });
</script>

@yield('footer')