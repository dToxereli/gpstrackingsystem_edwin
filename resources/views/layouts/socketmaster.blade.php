<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FiveOne Socket.io</title>
</head>
<body>
@yield('content')

<script type="text/javascript" src= "{{ asset('jQuery/jquery-3.2.1.min.js') }}"></script>
@yield('footer')
</body>
</html>