<div>
    @include('components.flash')
    <div>
        @yield('header')
    </div>

    <div>
        @yield('body')
    </div>
        
    <div>    
        @yield('footer')
    </div>
</div>