<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Setcustomtimeonlogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_log_entries', function (Blueprint $table) {
            $table->dateTime('currenttime')->after('workingtime');
        });

        Schema::table('terminal_duration_logs', function (Blueprint $table) {
            $table->dateTime('currenttime')->after('stop_time');
        });

        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->dateTime('currenttime')->after('longitude');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_log_entries', function (Blueprint $table) {
            $table->dropColumn('currenttime');
        });

        Schema::table('terminal_duration_logs', function (Blueprint $table) {
            $table->dropColumn('currenttime');
        });

        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->dropColumn('currenttime');
        });       
    }
}
