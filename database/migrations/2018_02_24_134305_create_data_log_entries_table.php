<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataLogEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_log_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imei');
            $table->double('latitude',9,6)->nullable();
            $table->double('longitude',8,6)->nullable();
            $table->double('altitude')->nullable();
            $table->float('distance',9,3)->nullable();
            $table->float('speed',6,3)->nullable();
            $table->float('oil1',5,2)->nullable();
            $table->float('oil2',5,2)->nullable();
            $table->float('temperature',6,2)->nullable();
            $table->float('workingtime',6,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_log_entries');
    }
}
