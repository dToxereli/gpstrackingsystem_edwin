<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalDurationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminal_duration_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imei');
            $table->double('start_latitude',9,6)->nullable();
            $table->double('start_longitude',8,6)->nullable();
            $table->dateTime('start_time');
            $table->double('stop_latitude',9,6)->nullable();
            $table->double('stop_longitude',8,6)->nullable();
            $table->dateTime('stop_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terminal_duration_logs');
    }
}
