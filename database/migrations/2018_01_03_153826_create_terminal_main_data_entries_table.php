<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalMainDataEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminal_main_data_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imei');
            $table->float('distance',9,3);
            $table->float('speed',6,3);
            $table->float('oil1',5,2)->nullable();
            $table->float('oil2',5,2)->nullable();
            $table->float('temperature',6,2)->nullable();
            $table->float('workingtime',6,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::dropIfExists('terminal_main_data_entries');
    }
}
