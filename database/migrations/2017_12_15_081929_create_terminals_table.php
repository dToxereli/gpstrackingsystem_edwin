<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terminals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone')->unique()->nullable();
            $table->string('identifier')->nullable();
            $table->string('imei')->unique()->nullable();
            $table->string('description')->nullable();
            $table->integer('client_id')->nullable()->unsigned();
            $table->integer('terminal_group_id')->nullable()->unsigned();
            $table->integer('terminal_configuration_id')->nullable()->unsigned();
            $table->date('expiry_date');
            $table->string('model');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terminals');
    }
}
