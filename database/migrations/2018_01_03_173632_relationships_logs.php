<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelationshipsLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('terminal_location_entries', function (Blueprint $table) {
            $table->foreign('imei')->references('imei')->on('terminals')->onDelete('cascade');
        });

        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->foreign('imei')->references('imei')->on('terminals')->onDelete('cascade');
        });

        Schema::table('terminal_main_data_entries', function (Blueprint $table) {
            $table->foreign('imei')->references('imei')->on('terminals')->onDelete('cascade');
        });

        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->foreign('keyword')->references('keyword')->on('terminal_event_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('terminal_location_entries', function (Blueprint $table) {
            $table->dropForeign('imei');
        });
        
        Schema::table('terminal_event_entries', function (Blueprint $table) {
            $table->dropForeign('imei');
        });

        Schema::table('terminal_main_data_entries', function (Blueprint $table) {
            $table->dropForeign('imei');
        });

        Schema::table('keyword', function (Blueprint $table) {
            $table->dropForeign('imei');
        });
    }
}