<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Relationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('terminals', function (Blueprint $table) {
            $table->foreign('client_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('terminal_group_id')->references('id')->on('terminal_groups')->onDelete('set null');
            $table->foreign('terminal_configuration_id')->references('id')->on('terminal_configurations')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('terminals', function (Blueprint $table) {
            $table->dropForeign('client_id');
            $table->dropForeign('terminal_group_id');
            $table->dropForeign('terminal_configuration_id');
        });
    }
}
