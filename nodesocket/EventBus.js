'use strict'

const events = require('events');
class EventBus extends events{}
const eventBus = new EventBus();

class EventSystem 
{
    static Bus()
    {
        if (!eventBus) {
            eventBus = new EventBus();
        }
        return eventBus;
    }
}

module.exports = EventSystem;