'use strict';
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

const tsFormat = () => (new Date().toISOString());
let logpath = __dirname + `/logs/socketserver.out.log`;
let errorLogpath = __dirname + `/logs/socketserver.err.log`;
var myFormat = printf(info => {
    return `[${info.timestamp}] ${info.label}.${info.level.toUpperCase()}: ${info.message} ` 
    + (info.hasOwnProperty('details') ? JSON.stringify(info.details) : '') 
    + (info.hasOwnProperty('stacktrace') ? '\n[Stacktrace]\n'+info.stacktrace : '');
});
var logformat = combine(
    label({ label: process.env.APP_ENV || 'local' }),
    timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }),
    myFormat
);
// var logformat = format.combine(
//     format.timestamp(),
//     format.json()
// );

var options = {
    info: {
        level: 'info',
        filename: logpath,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
        format: logformat
    },
    error: {
        level: 'error',
        filename: errorLogpath,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
        format: logformat
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
        format: logformat
    },
};

var Logger = createLogger({
    transports: [
        new transports.File(options.info),
        new transports.File(options.error),
        new transports.Console(options.console)
    ],
    exceptionHandlers: [
        new transports.File(options.error)
    ],
    exitOnError: false, // do not exit on handled exceptions
});

Logger.stream = {
    write: function (message, encoding) {
        Logger.info(message);
    },
};

module.exports = Logger;