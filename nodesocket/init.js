#!/usr/bin/env node
// First line is to ensure it runs with nodejs

// Next line is to set javascript syntax rules
'use strict';

require('dotenv').config('../');

var eventBus = require('./EventBus').Bus();
const Server = require('./TCPServer');
var Logger = require('./Logger');

const IP = process.argv[2] || "127.0.0.1";
const PORT = parseInt(process.argv[3] || "31337");
const PROTOCOL = process.argv[4] || "TK 103A";

let server = new Server(PORT, IP);

server.start(() => {
    Logger.info(`TCP Server for ${PROTOCOL} started at: ${IP}:${PORT}`);
});

function exitHandler(options, err) {
    Logger.info("Exiting ...");

    if (options.cleanup) {
        server.stop((err, response, body) => {
            if (err) {
                Logger.error(err, {details: body});
            } else {
                Logger.info(`TCP Server for ${PROTOCOL} at: ${IP}:${PORT} was stopped`);
            }

            if (options.exit) {
                process.exit(0);
            }
        });
    }
    
    if (err) {
        Logger.error("Error triggered application exit",{details: err});
        if (options.exit) {
            process.exit(1);
        }
    }
}

process.on('exit', exitHandler.bind(null, {exit: false, cleanup: false}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true, cleanup: true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit:true, cleanup: true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit:true, cleanup: true}));

//catches uncaught exceptions
// process.on('uncaughtException', exitHandler.bind(null, {exit:false, cleanup: true}));