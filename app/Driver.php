<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [
        'name','phone','email'
    ];

    public function owner() 
    {
        return $this->belongsTo('App\User','owner_id');
    }

    public function terminal() 
    {
        return $this->hasOne('App\Terminal','driver_id','id');
    }

    public function setTerminal($terminal)
    {
        if (isset($terminal->driver)) {
            $terminal->driver->removeTerminal();
        }
        $this->terminal()->save($terminal);
        $terminal->driver()->save($this);
    }

    public function removeTerminal()
    {
        $terminal = $this->terminal;
        if (isset($terminal)) {
            $terminal->driver_id = null;
            $terminal->save();
        }
        $this->imei = null;
        $this->save();
    }
}
