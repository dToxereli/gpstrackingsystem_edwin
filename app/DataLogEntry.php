<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataLogEntry extends Model
{
    protected $fillable = [
        'driver_id',
        'imei',
        'latitude',
        'longitude',
        'altitude',
        'distance',
        'speed',
        'oil1',
        'oil2',
        'temperature',
        'workingtime',
        'currenttime'
    ];
    
    public function terminal()
    {
        return $this->belongsTo('App\Terminal','imei','imei');
    }

    public function scopeDateBetween($query, $start, $end) {
        return $query->whereBetween('created_at', [$start, $end]);
    }
}
