<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerminalConfiguration extends Model
{
    protected $fillable = ['configuration_name', 'file_name'];

    public function terminals()
    {
        return $this->hasMany('App\Terminal','terminal_configuration_id');
    }
}
