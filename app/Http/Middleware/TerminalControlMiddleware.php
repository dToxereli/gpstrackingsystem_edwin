<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class TerminalControlMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->hasPermissionTo('Terminal Control') || Auth::user()->hasPermissionTo('Terminal Control - All Terminals'))
        {
            return $next($request);
        }

        abort('401');
    }
}
