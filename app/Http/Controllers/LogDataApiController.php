<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use App\DataLogEntry;
use App\TerminalEventEntry;
use App\Terminal;
use App\Driver;

class LogDataApiController extends Controller
{
    /**
     * Logs main data about a terminal current location, along with its status
     * 
     * @param \Illuminate\Http\Request request
     */
    public function LogMainData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'imei' => 'required|exists:terminals.imei',
            'speed' => 'nullable|double',
            'latitude' => 'nullable|double|max:90|min:-90',
            'longitude' => 'nullable|double|max:180|min:-180'
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'There were error in the received data', 'errors' => $validator->errors()], 422);
        }

        $data = $request->only('imei','speed','distance','oil1','oil2','temperature','workingtime','latitude','longitude','altitude','currenttime');
        $driver = Driver::where('imei',$data['imei'])->first();
        $data['driver_id'] = isset($driver) ? $driver->id : null;
        DataLogEntry::create($data);

        return response()->json(['msg' => 'Data logged successfully'], 200);
    }

    public function LogEventData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'imei' => 'required|exists:terminals.imei',
            'keyword' => 'required|exists:terminal_event_types.keyword',
            'latitude' => 'nullable|double|max:90|min:-90',
            'longitude' => 'nullable|double|max:180|min:-180'
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'There were error in the received data', 'errors' => $validator->errors()], 422);
        }

        $data = $request->only('imei','keyword','latitude','longitude','currenttime');
        $driver = Driver::where('imei',$data['imei'])->first();
        $data['driver_id'] = isset($driver) ? $driver->id : null;
        TerminalEventEntry::create($data);

        return response()->json(['msg' => 'Data logged successfully'], 200);
    }

    public function UpdateLiveData(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'imei' => 'required|exists:terminals.imei',
        //     'speed' => 'nullable|double',
        //     'latitude' => 'nullable|double|max:90|min:-90',
        //     'longitude' => 'nullable|double|max:180|min:-180'
        // ]);
        return response()->json(['data' => json_encode($request)], 200);
    }
}
