<?php

namespace App\Http\Controllers;

use Auth;
use Validator;

use App\TerminalGroup;
use App\Terminal;
use App\User;

use Illuminate\Http\Request;

class TerminalGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $terminal_groups = $user->terminal_groups;
        return view('home.terminalgroups.index',compact('terminal_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('home.terminalgroups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'name'=>'required|unique:terminal_groups|max:30',
        ]);
        if($validator->fails()) {
            return view('home.terminalgroups.create')->withErrors($validator)
            ->with('flash_message',[
                'message' => 'Looks like there was an error in your input',
                'title' => 'Invalid input',
                'sender' => 'TGROUP',
                'type' => 'ERROR'
            ]);
        }
        $input = $request->only('name');
        $terminal_group = TerminalGroup::create($input);
        $terminal_group->user()->associate($user);
        $terminal_group->save();
        $terminal_groups = $user->terminal_groups;
        return view('home.terminalgroups.index',compact('terminal_groups'))
        ->with('flash_message',[
            'message' => 'You created terminal group ' . $terminal_group->name,
            'title' => 'Success',
            'sender' => 'TGROUP',
            'type' => 'INFO'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TerminalGroup  $terminalGroup
     * @return \Illuminate\Http\Response
     */
    public function show(TerminalGroup $terminalGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $terminal_group = TerminalGroup::findOrFail($id);
        return view('home.terminalgroups.edit', compact('terminal_group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $terminal_group = TerminalGroup::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:30|string|unique:terminal_groups,name,'.$terminal_group->id,
        ]);
        if($validator->fails()) {
            return view('home.terminalgroups.edit', compact('terminal_group'))->withErrors($validator)
            ->with('flash_message',[
                'message' => 'Looks like there was an error in your input',
                'title' => 'Invalid input',
                'sender' => 'TGROUP',
                'type' => 'ERROR'
            ]);
        }
        $input = $request->only('name');        
        $terminal_group->fill($input)->save();
        $terminal_groups = $user->terminal_groups;
        return view('home.terminalgroups.index',compact('terminal_groups'))
            ->with('flash_message',[
                'message' => 'You modified terminal group ' . $terminal_group->name,
                'title' => 'Success',
                'sender' => 'TGROUP',
                'type' => 'INFO'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $terminal_group = TerminalGroup::findOrFail($id);
        $terminal_group->delete();
        $user = Auth::user();
        $terminal_groups = $user->terminal_groups;
        return view('home.terminalgroups.index',compact('terminal_groups'))->with('flash_message',[
            'message' => 'You deleted terminal group ' . $terminal_group->name,
            'title' => 'Success',
            'sender' => 'TGROUP',
            'type' => 'INFO'
        ]);
    }
}
