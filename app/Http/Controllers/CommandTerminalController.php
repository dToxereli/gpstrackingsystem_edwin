<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Validation\Rule;
use Validator;
use App\Utilities\GPSProtocolParser;
use App\Events\SendCommandToTerminal;
use Log;

class CommandTerminalController extends Controller
{
    public function show($id)
    {
        $commands = config('gpsprotocol.webapp');
        if (!array_key_exists($id, $commands)) {
            return response()->json(['msg' => 'The command does not exist'], 404);
        }

        $description = $commands[$id]['description'];
        $terminals = Auth::user()->adminTerminals;
        $viewpath = "map.commands.confirm";
        switch ($id) {
            case 'settimezone':
                $viewpath = "map.commands.time";
                break;
            case 'trackbytime':
                $viewpath = "map.commands.trackbytime";
                break;
            case 'trackbydistance':
                $viewpath = "map.commands.trackbydistance";
                break;
            case 'setoverspeedalarm':
                $viewpath = "map.commands.speed";
                break;
            case 'setgeofence':
                $viewpath = "map.commands.geofence";
                break;
            default:
                break;
        }

        return view($viewpath)
            ->with('terminals', $terminals)
            ->with('command', $id)
            ->with('description', $description);
    }

    public function send(Request $request, $id)
    {
        $commands = config('gpsprotocol.webapp');

        if (!array_key_exists($id, $commands)) {
            return response()->json(['msg' => 'The command does not exist'], 404);
        }

        $validationrules = array();
        $durations = [
            "10s","15s","20s","30s","1m","5m","10m","30m","1h","5h","10h","24h"
        ];
        $speeds = [
            "010","020","030","040","050","060","080","100","120","150","180","200","220","250","300"
        ];
        $distances = [
            "0050m","0100m","0200m","0500m","0800m","1km","2km","5km","10km"
        ];
        switch ($id) {
            case 'settimezone':
                $validationrules = [
                    'offset' => 'required|numeric|min:-12|max:12',
                    'terminals' => 'required|exists:terminals,id'
                ];
                break;
            case 'trackbytime':
                $validationrules = [
                    'duration' => [
                        'required',
                        Rule::in($durations),
                    ],
                    'terminals' => 'required|exists:terminals,id'
                ];
                break;
            case 'trackbydistance':
                $validationrules = [
                    'distance' => [
                        'required',
                        Rule::in($distances),
                    ],
                    'terminals' => 'required|exists:terminals,id'
                ];
                break;
            case 'setoverspeedalarm':
                $validationrules = [
                    'speed' => [
                        'required',
                        Rule::in($speeds),
                    ],
                    'terminals' => 'required|exists:terminals,id'
                ];
                break;
            case 'setgeofence':
                $validationrules = [
                    'latitude_topleft' => 'required|numeric|min:-90|max:90',
                    'longitude_topleft' => 'required|numeric|min:-180|max:180',
                    'latitude_bottomright' => 'required|numeric|min:-90|max:90',
                    'longitude_bottomright' => 'required|numeric|min:-180|max:180',
                    'terminals' => 'required|exists:terminals,id'
                ];
                break;
            default:
                $validationrules = [
                    'terminals' => 'required|exists:terminals,id'
                ];
                break;
        }

        
        $validator = Validator::make($request->all(), $validationrules);

        if ($validator->fails()) {

            $description = $commands[$id]['description'];
            $terminals = Auth::user()->adminTerminals;
            $viewpath = "map.commands.confirm";
            switch ($id) {
                case 'settimezone':
                    $viewpath = "map.commands.time";
                    break;
                case 'trackbytime':
                    $viewpath = "map.commands.trackbytime";
                    break;
                case 'trackbydistance':
                    $viewpath = "map.commands.trackbydistance";
                    break;
                case 'setoverspeedalarm':
                    $viewpath = "map.commands.speed";
                    break;
                case 'setgeofence':
                    $viewpath = "map.commands.geofence";
                    break;
                default:
                    break;
            }

            return view($viewpath)
                ->with('terminals', $terminals)
                ->with('command', $id)
                ->with('description', $description)
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'SYSTEM',
                    'type' => 'ERROR'
            ]);
        }

        $parameter = "";
        $terminals = Auth::user()->adminTerminals->whereIn('id',$request['terminals']);

        switch ($id) {
            case 'settimezone':
                $parameter = $request["offset"] > 0 ? "+".$request["offset"] : $request["offset"];
                break;
            case 'trackbytime':
                $parameter = $request["duration"];
                break;
            case 'trackbydistance':
                $parameter = $request["distance"];
                break;
            case 'setoverspeedalarm':
                $parameter = $request["speed"];
                break;
            case 'setgeofence':
                $parameter = $request['latitude_topleft'].','.$request['longitude_topleft'].','.$request['latitude_bottomright'].','.$request['longitude_bottomright'];
                break;
            default:
                break;
        }
        
        foreach ($terminals as $terminal) {
            $data = [
                'terminal' => $terminal,
                'command' => $id,
                'parameter' => $parameter
            ];
            $message = GPSProtocolParser::Compose($data, $terminal->model);
            $sockets = $terminal->tcpserverconnections->pluck('name');
            event(new SendCommandToTerminal($message, $sockets));
        }

        return view('map.commands.response');
    }
}
