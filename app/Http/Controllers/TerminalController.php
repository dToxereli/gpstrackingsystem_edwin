<?php

namespace App\Http\Controllers;

use Auth;
use Validator;

use App\User;
use App\Terminal;
use App\TerminalGroup;
use App\TerminalConfiguration;

use App\Events\AddTerminal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TerminalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terminals = Terminal::all();        
        return view('admin.terminals.index', compact('terminals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $configurations = TerminalConfiguration::where('type','=','global')->get();
        return view('admin.terminals.create', compact('configurations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $validator = Validator::make($request->all(), [
            'phone' => 'required|regex:/^[+]?[0-9]{10,12}$/|unique:terminals',
            'imei' => 'required|regex:/^[0-9]{15,16}$/|unique:terminals',
            'expiry_date' => 'required|date|max:255',
            'model' => 'required',
            'identifier' => 'nullable|string|max:100',
            'description' => 'nullable|string|max:255',
            'configuration' => 'required|integer',
        ]);

        if($validator->fails()) {
            $request->flash();
            $configurations = TerminalConfiguration::where('type','=','global')->get();
            return view('admin.terminals.create', compact('configurations'))
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'TERMINAL',
                    'type' => 'ERROR'
                ]);
        }
        $input = $request->only('phone','imei','expiry_date','model','identifier','description');        
        $terminal = Terminal::create($input);  
        
        $configinput = $request['configuration'];
        if(isset($configinput))  
        {
            if($configinput != -1)
            {
                $config = TerminalConfiguration::where('id', '=' ,$configinput)->firstOrFail();
                if(isset($config)) {
                    $terminal->configuration()->associate($config);
                }
            }            
        }

        $terminal->save();
        event(new AddTerminal($terminal));
        $terminals = Terminal::all();        
        return view('admin.terminals.index', compact('terminals'))
            ->with('flash_message',[
                'message' => 'You added a terminal',
                'title' => 'Success',
                'sender' => 'TERMINAL',
                'type' => 'INFO'
            ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Terminal  $terminal
     * @return \Illuminate\Http\Response
     */
    public function edit(Terminal $terminal)
    {
        $configurations = TerminalConfiguration::where('type','=','global')->get();
        return view('admin.terminals.edit', compact('terminal','configurations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Terminal  $terminal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Terminal $terminal)
    {        
        $validator = Validator::make($request->all(), [
            'phone' => 'nullable|regex:/^[+]?[0-9]{10,12}$/|unique:terminals,phone,'.$terminal->id,
            'imei' => 'nullable|regex:/^[0-9]{15,16}$/|unique:terminals,imei,'.$terminal->id,
            'expiry_date' => 'nullable|date|max:255',
            'model' => 'nullable',
            'identifier' => 'nullable|string|max:100',
            'description' => 'nullable|string|max:255',
            'configuration' => 'required|integer',
        ]);

        if($validator->fails()) {
            $request->flash();
            $configurations = TerminalConfiguration::where('type','=','global')->get();
            return view('admin.terminals.edit', compact('terminal','configurations'))
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'TERMINAL',
                    'type' => 'ERROR'
                ]);
        }

        $input = $request->only('phone','imei','expiry_date','model','identifier','description');
        $configinput = $request['configuration'];        
        if(isset($configinput)) {
            if ($configinput == -1) {
                $terminal->configuration()->dissociate();   
            } else {
                $config = TerminalConfiguration::where('id', '=' , $configinput)->firstOrFail();
                if (isset($config)) {
                    $terminal->configuration()->associate($config);
                }
            }                 
        }
        $terminal->fill($input)->save();
        $terminals = Terminal::all();        
        return view('admin.terminals.index', compact('terminals'))
            ->with('flash_message',[
                'message' => 'You modified the terminal',
                'title' => 'Success',
                'sender' => 'TERMINAL',
                'type' => 'INFO'
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Terminal  $terminal
     * @return \Illuminate\Http\Response
     */
    public function editUser($id)
    {
        $terminal = Terminal::findOrFail($id);
        $users = User::role('Client')->get();
        return view('admin.terminals.assign', compact('terminal','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Terminal  $terminal
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request, $id)
    {
        $terminal = Terminal::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'user' => 'required|integer',
        ]);
        if($validator->fails()) {
            $request->flash();
            $users = User::role('Client')->get();
            return view('admin.terminals.assign', compact('terminal','users'))
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'TERMINAL',
                    'type' => 'ERROR'
                ]);
        }
        $input = $request['user'];        
        if ($input == -1) {
            $terminal->user()->dissociate();
        } else {
            $user = User::findOrFail($input);
            $terminal->user()->associate($user);
        }
        $terminal->save();
        $terminals = Terminal::all();        
        return view('admin.terminals.index', compact('terminals'))
            ->with('flash_message',[
                'message' => 'You changed the terminal\'s owner',
                'title' => 'Success',
                'sender' => 'TERMINAL',
                'type' => 'INFO'
            ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Terminal  $terminal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Terminal $terminal)
    {
        $terminal->delete();
        $terminals = Terminal::all();
        return view('admin.terminals.index', compact('terminals'))
        ->with('flash_message',[
            'message' => 'You deleted the terminal',
            'title' => 'Success',
            'sender' => 'TERMINAL',
            'type' => 'INFO'
        ]);
    }
}
