<?php

namespace App\Http\Controllers;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Terminal;
use App\TerminalMainDataEntry;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $pd = $user->lastdashboardcheck;
        $d = date('Y-m-d h:i:s');        
        $user->lastdashboardcheck = $d;
        $user->save();
        return view('home.index');
    }

    public function dashboard()
    {
        $now = Carbon::now();
        $user = Auth::user();
        $terminals = NULL;
        if ($user->hasPermissionTo('Terminal Control - All Terminals')) {
            $terminals = Terminal::all();
        }
        else if ($user->hasPermissionTo('Terminal Control')) {
            $terminals = $user->terminals;
        }
        if ($terminals === NULL) {
            $y = date('Y');
            $md = date('M d');
            return view('home.dashboard')
                ->with('data', array())
                ->with('y', $y)
                ->with('md', $md);
        }
        $imeis = $terminals->pluck('imei')->toArray();
        $dataset = DB::table('terminal_event_entries')
                ->join('terminal_event_types','terminal_event_entries.keyword','=','terminal_event_types.keyword')
                ->select('terminal_event_entries.*', 'terminal_event_types.description')
                ->whereIn('terminal_event_entries.imei', $imeis )
                ->whereDate('terminal_event_entries.created_at' ,'>',$now->subDays(3)->toDateString())
                ->limit(10)
                ->orderBy('created_at','desc')
                ->get();
        
        if (!isset($dataset)) {
            $y = date('Y');
            $md = date('M d');
            return view('home.dashboard')
                ->with('data', null)
                ->with('y', $y)
                ->with('md', $md);
        }

        $displayData = array();
        foreach ($dataset as $data) {
            $entry = array();
            $entry['age'] = Carbon::createFromTimeStamp(strtotime($data->created_at))->diffForHumans();
            $entry['name'] = $terminals->where('imei',$data->imei)->first()->display;
            $entry['date'] = $data->created_at;
            $entry['description'] = $data->description;
            array_push($displayData,$entry);
        }

        $y = date('Y');
        $md = date('M d');
        return view('home.dashboard')
                ->with('data', $displayData)
                ->with('y', $y)
                ->with('md', $md);
    }
}
