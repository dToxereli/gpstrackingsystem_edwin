<?php

namespace App\Http\Controllers;

use Auth;
use Validator;

use App\Terminal;
use App\TerminalGroup;
use App\User;

use Illuminate\Http\Request;

class UserTerminalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $terminals = $user->terminals;
        return view('home.terminals.index',compact('terminals'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $terminal_groups = $user->terminal_groups;
        $terminal = Terminal::findOrFail($id);
        return view('home.terminals.edit',compact('terminal','terminal_groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $terminal = Terminal::findOrFail($id);

        $validator = Validator::make($request->all(), [            
            'identifier' => 'nullable|string|max:30',
            'description' => 'nullable|string|max:100',
            'terminalgroup' => 'required|integer',
        ]);

        if($validator->fails()) {            
            $terminal_groups = $user->terminal_groups;            
            return view('home.terminals.edit',compact('terminal','terminal_groups'))
                ->withErrors($validator)
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'TERMINAL',
                    'type' => 'ERROR'
                ]);
        }

        $input = $request->only('identifier','description');
        $group_input = $request['terminalgroup'];
        if(isset($group_input)) {
            if ($group_input == -1) {
                $terminal->terminal_group()->dissociate();   
            } else {
                $terminal_group = TerminalGroup::findOrFail($group_input);
                if (isset($terminal_group)) {
                    $terminal->terminal_group()->associate($terminal_group);
                }
            }                 
        }
        $terminal->fill($input)->save();
        $terminals = $user->terminals;
        return view('home.terminals.index',compact('terminals'))
        ->with('flash_message',[
            'message' => 'You successfully modified the terminal',
            'title' => 'Success',
            'sender' => 'TERMINAL',
            'type' => 'INFO'
        ]);
    }
}
