<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MapSocketServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $sockets = collect($config['socket']['web']);

        return view('server.webserver.index')->with('sockets', $sockets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('server.webserver.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valdiator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'ip' => 'required|string|regex:/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/',
            'port' => 'required|integer|max:65535|min:2000'
        ]);

        if ($valdiator->fails()) {
            return view('server.webserver.create')
                ->withErrors($valdiator->errors())
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'SOCKSERVER',
                    'type' => 'ERROR'
                ]);
        }

        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $sockets = collect($config['socket']['web']);

        $exists = $sockets->where('name', $request['name'])->first();
        
        if (isset($exists)) {
            $sockets = $sockets->map(function($socket) use ($request) { 
                if($socket['name'] == $request['name']) {
                    $socket['ip'] = $request['ip']; 
                    $socket['port'] = $request['port']; 
                }
                return $socket;
            });
        } else {
            $sockets = $sockets;
            $sockets->push([
                'name' => $request['name'],
                'ip' => $request['ip'],
                'port' => $request['port']
            ]);
        }

        $config['socket']['web'] = $sockets->all();
        $json = json_encode($config, JSON_PRETTY_PRINT);
        Storage::disk('socketserver')->put('system.json',$json);

        return view('server.webserver.index')
            ->with('sockets', $sockets)
            ->with('flash_message',[
                'message' => 'You created live map server ' . $request['name'],
                'title' => 'Success',
                'sender' => 'SOCKSERVER',
                'type' => 'INFO'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $sockets = collect($config['socket']['web']);
        $exists = $sockets->where('name', $id)->first();

        if ($exists) {
            return view('server.webserver.edit')->with('socket', $exists);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $sockets = collect($config['socket']['web']);
        if ($id == 'main') {
            return view('server.tcpserver.index')
            ->with('sockets', $sockets)
            ->with('flash_message',[
                'message' => 'You can\'t modify that server',
                'title' => 'Restricted action',
                'sender' => 'SOCKSERVER',
                'type' => 'ERROR'
            ]);
        }
        
        $exists = $sockets->where('name', $id)->first();

        if ($exists) {
            return view('server.webserver.edit')->with('socket', $exists);
        } else {
            return view('server.webserver.index')
            ->with('sockets', $sockets)
            ->with('flash_message',[
                'message' => 'That server does not exist',
                'title' => 'Invalid input',
                'sender' => 'SOCKSERVER',
                'type' => 'ERROR'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valdiator = Validator::make($request->all(), [
            'name' => 'required|string|max:30',
            'ip' => 'required|string|regex:/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/',
            'port' => 'required|integer|max:65535|min:2000'
        ]);

        if ($valdiator->fails()) {
            return view('server.webserver.create')
                ->withErrors($valdiator->errors())
                ->with('flash_message',[
                    'message' => 'Looks like there was an error in your input',
                    'title' => 'Invalid input',
                    'sender' => 'SOCKSERVER',
                    'type' => 'ERROR'
                ]);
        }

        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $sockets = collect($config['socket']['web']);

        if ($id == 'main') {
            return view('server.webserver.index')
            ->with('sockets', $sockets)
            ->with('flash_message',[
                'message' => 'You can\'t modify that server',
                'title' => 'Restricted action',
                'sender' => 'SOCKSERVER',
                'type' => 'ERROR'
            ]);
        }

        $exists = $sockets->where('name', $request['name'])->first();

        if (isset($exists)) {
            $sockets = $sockets->map(function($socket) use ($request, $id) { 
                if($id != 'main' && $socket['name'] == $id) {
                    $socket['ip'] = $request['ip']; 
                    $socket['port'] = $request['port']; 
                }
                return $socket;
            });
        }

        $config['socket']['web'] = $sockets->all();
        $json = json_encode($config, JSON_PRETTY_PRINT);
        Storage::disk('socketserver')->put('system.json',$json);

        return view('server.webserver.index')
            ->with('sockets', $sockets)
            ->with('flash_message',[
                'message' => 'You changed live map server ' . $request['name'],
                'title' => 'Success',
                'sender' => 'SOCKSERVER',
                'type' => 'INFO'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $json = Storage::disk('socketserver')->get('system.json');
        $config = json_decode($json, true);
        $sockets = collect($config['socket']['web']);

        if ($id == 'main') {
            return view('server.webserver.index')
            ->with('sockets', $sockets)
            ->with('flash_message',[
                'message' => 'You can\'t delete that server',
                'title' => 'Restricted action',
                'sender' => 'SOCKSERVER',
                'type' => 'ERROR'
            ]);
        }

        $sockets = $sockets->map(function($socket) use ($id) { 
            if($id != 'main' && $socket['name'] == $id) {
                return null;
            }
            return $socket;
        })->filter();
        $config['socket']['web'] = $sockets->all();
        $json = json_encode($config, JSON_PRETTY_PRINT);
        Storage::disk('socketserver')->put('system.json',$json);

        return view('server.webserver.index')
            ->with('sockets', $sockets)
            ->with('flash_message',[
                'message' => 'You deleted live map server ' . $id,
                'title' => 'Success',
                'sender' => 'SOCKSERVER',
                'type' => 'INFO'
            ]);
    }
}
