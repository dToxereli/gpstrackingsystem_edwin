<?php

namespace App\Http\Controllers;

use Auth;
use App\Terminal;
use App\MapAuthKey;
use App\TerminalAccessLog;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Events\MapAccess;
use Illuminate\Support\Facades\Storage;

class MapController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $terminals;
        $description = null;
        if ($user->hasPermissionTo('Terminal Control - All Terminals')) {
            $terminals = Terminal::all();
            $description = 'Admin access';
        }
        else if ($user->hasPermissionTo('Terminal Control')) {
            $terminals = $user->terminals;
            $description = 'User access';
        }

        foreach ($terminals as $terminal) {
            $accesslog = TerminalAccessLog::create(['description' => $description]);
            $accesslog->terminal()->associate($terminal);
            $accesslog->user()->associate($user);
            $accesslog->save();
        }
        
        $sockets = collect(config('appsettings.web_sockets'));

        $apikey = config('appsettings.api_keys.google_maps');

        return view('map.index',compact('terminals'))
            ->with('apikey', $apikey)
            ->with('socket', $sockets->random());
    }
}