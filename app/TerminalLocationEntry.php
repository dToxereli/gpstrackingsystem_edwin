<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TerminalLocationEntry extends Model
{
    public function terminal()
    {
        return $this->belongsTo('App\Terminal','imei','imei');
    }

    public function scopeDateBetween($query, $start, $end) {
        return $query->whereBetween('created_at', [$start, $end]);
    }
}
