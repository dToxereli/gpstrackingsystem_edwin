<?php
namespace App\Utilities;

use Illuminate\Support\Facades\DB;
use App\Utilities\ReportQueryUtility;
use Carbon\Carbon;

use App\Terminal;
use App\TerminalEventType;

class EventData
{
    protected $db;
    protected $querystring;
    protected $types;
    protected $terminals;
    public $data;

    function __construct() {
        $this->querystring = "";
        $this->db = DB::table('terminal_event_entries');
    }
    /**
     * Runs the query and sets the data in $data variable
     * Returns the data that has been collected
     * @return App\Utilities\PerformanceData
     */
    public function run()
    {
        $this->data = $this->db
                ->groupBy('imei')
                ->groupBy('keyword')
                ->groupBy('datetime')
                ->orderBy('datetime','asc')
                ->get();
        return $this;
    }

    /**
     * Prepares the query from the provided input
     * Returns the data that has been collected
     * @return App\Utilities\PerformanceData
     */
    public function fill($input)
    {
        $this->querystring = ReportQueryUtility::$datesummaries[$input['summary']];
        $this->querystring .= ',COUNT(*) AS frequency,AVG(longitude) AS longitude,AVG(latitude) AS latitude';
        $this->db->select(DB::raw('imei,keyword,'.$this->querystring))
                ->havingRaw('datetime BETWEEN TIMESTAMP("'.$input['start_date'].'","'.$input['start_time'].'") AND TIMESTAMP("'.$input['end_date'].'","'.$input['end_time'].'")');
        return $this;
    }

    /**
     * Prepares the chart array from the provided input
     * Returns the data that has been collected
     * 
     * @param  string  $fields
     * @return array
     */
    public function getChartData($field)
    {
        $groupings = array();
        $header = [['datetime','Date']];
        $f;
        if ($field == 'terminals') {
            $terminals = $this->terminals->pluck('display','imei')->toArray();
            foreach ($terminals as $key => $value) {
                array_push($header, ['number',$value]);
                array_push($groupings, $key);
            }
            $f = 'imei';
        } else {
            $eventtypes = $this->types->pluck('description','keyword')->toArray();
            foreach ($eventtypes as $key => $value) {
                array_push($header, ['number',$value]);
                array_push($groupings, $key);
            }
            $f = 'keyword';
        }

        $datatable = array();
        $this->data->groupBy('datetime')->map(function($row, $date) use ($f, $groupings, &$datatable) {
            $rowdata = [$date];
            for ($i=0; $i < count($groupings); $i++) {
                $entry = $groupings[$i];
                $fielddata = $row->firstWhere($f,$entry);
                $data = (isset($fielddata)) ? $fielddata->frequency : 0;
                array_push($rowdata, $data);
            }
            array_push($datatable, $rowdata);
            return $row;
        });
        $r = [
            'coldefs' => $header,
            'data' => $datatable
        ];
        return $r;
    }

    /**
     * Prepares the chart summary array from the provided input
     * Returns the data that has been collected
     * 
     * @param  string  $fields
     * @return array
     */
    public function getChartSummaryData($field)
    {
        $terminals = $this->terminals->pluck('display','imei')->toArray();
        $events = $this->types->pluck('description','keyword')->toArray();
        $header = array_values($terminals);
        $header = [['string', ucfirst($field)], ['number', 'Frequency']];
        $f = ($field == 'terminal') ? 'imei' : $field;
        $f = ($field == 'event') ? 'keyword' : $f;
        $datatable = array();
        $this->data->groupBy($f)->map(function($row, $fkey) use ($terminals, $events, &$datatable, $field) {
            $rowdata;
            if ($field == 'terminal') {
                $rowdata = [$terminals[$fkey]];
            } else if ($field == 'event') {
                $rowdata = [$events[$fkey]];
            } else {
                $rowdata = [$fkey];
            }
            array_push($rowdata, $row->sum('frequency'));
            array_push($datatable, $rowdata);
        });
        $r = [
            'coldefs' => $header,
            'data' => $datatable
        ];
        return $r;
    }

    public function getXLSData()
    {
        $terminals = $this->terminals->pluck('display','imei')->toArray();
        $eventtypes = $this->types->pluck('description','keyword')->toArray();
        $xlsheader = ['date','terminal','event','frequency','latitude','longitude'];
        $xlsdata = $this->data->map(function ($data) use ($terminals, $eventtypes, $xlsheader) {
            return [$data->datetime, $terminals[$data->imei], $eventtypes[$data->keyword], $data->frequency, $data->latitude, $data->longitude];
        })->toArray();
        array_unshift($xlsdata,$xlsheader);
        return $xlsdata;
    }

    /**
     * Prepares the chart array from the provided input
     * Returns the data that has been collected
     * 
     * @param  string  $fields
     * @return array
     */

    public function getLocationData ()
    {
        $terminals = $this->terminals->pluck('display','imei')->toArray();
        $types = $this->types->pluck('description','keyword')->toArray();
        $var = $this->data->map(function ($event, $key) use ($terminals, $types) {
            $r = [
                'terminal' => $terminals[$event->imei],
                'event' => $types[$event->keyword],
                'latitude' => $event->latitude,
                'longitude' => $event->longitude,
                'description' => '
                <div id="iw-container">
                <div class="iw-title">
                <div>'.$types[$event->keyword].'</div>
                <div>'.$terminals[$event->imei].'</div>
                <div>'.Carbon::createFromTimeStamp(strtotime($event->datetime))->toDayDateTimeString().'</div>
                </div>
                <div class="iw-content">
                <table class="center-block" width="100%">
                <tr> <th class="text-right"> Type:</th> <td>'.$types[$event->keyword].' </td> </tr>
                <tr> <th class="text-right"> Terminal:</th> <td>'.$terminals[$event->imei].' </td> </tr>
                <tr> <th class="text-right"> Frequency:</th> <td>'.$event->frequency.' </td> </tr>
                <tr> <th class="text-right"> Latitude:</th> <td>'.$event->latitude.' </td> </tr>
                <tr> <th class="text-right"> Longitude:</th> <td>'.$event->longitude.' </td> </tr>
                </table>
                </div>
                </div>'
            ];
            return $r;
        })->toArray();
        return $var;
    }

    /**
     * Used to set the terminals for which records are retrieved
     *
     * @param  App\Terminal[]  $terminals
     * @return App\Utilities\PerformanceData
     */
    public function setterminals($terminals)
    {
        $this->terminals = $terminals;
        $imeis = $terminals->pluck('imei')->toArray();
        $this->db->whereIn('imei', $imeis );
        return $this;
    }

    /**
     * Used to set the types for which records are retrieved
     *
     * @param  App\TerminalEventType  $keywords
     * @return App\Utilities\PerformanceData
     */
    public function settypes($types)
    {
        $this->types = $types;
        $keywords = $types->pluck('keyword')->toArray();
        $this->db->whereIn('keyword', $keywords );
        return $this;
    }

    /**
     * Used to set start and end date bounds for the data to retrieve
     * Returns this same object to support chaining
     * @param  Carbon\Carbon  $start
     * @param  Carbon\Carbon  $end
     * @return App\Utilities\PerformanceData
     */
    public function whereBetween($start, $end)
    {
        $this->db->havingRaw('datetime BETWEEN TIMESTAMP("'.$start.'") AND TIMESTAMP("'.$end.'")');
        return $this;
    }

    /**
     * Used to set the summary duration to be used
     * Returns this same object to support chaining
     *
     * @param  string  $summary
     * @return App\Utilities\PerformanceData
     */
    public function summary($summary)
    {
        $this->querystring .= ReportQueryUtility::$datesummaries[$input['summary']];
        return $this;
    }

    /**
     * Select ordering field and direction
     * Returns this same object to support chainign
     *
     * @param  string  $field
     * @param  int  $id
     * @return App\Utilities\PerformanceData
     */
    public function orderBy($field, $direction)
    {
        $this->db->orderBy($field, $direction);
        return $this;
    }

    /**
     * Selects fields with which to group the data
     *
     * @param  string  $field
     * @param  int  $id
     * @return App\Utilities\PerformanceData
     */
    public function groupBy($field)
    {
        $this->db->groupBy($field);
        return $this;
    }    
}