<?php
namespace App\Utilities;

use Illuminate\Support\Collection;

class ReportExport implements FromCollection
{
    public function collection($data, $header)
    {
        $arr = isset($header) ? array_merge($header, $data) : $data;
        return collect($arr);
    }
}